let
  pkgs = import ../pkgs/stable.nix;
in
  pkgs.stdenv.mkDerivation (
        (import ../src/basic.nix)
    //  (rec {
          name = "builder";

          buildInputs = [
            pkgs.git
            pkgs.coreutils
            pkgs.google-cloud-sdk
            pkgs.terraform
            pkgs.tflint
          ];
        })
  )
