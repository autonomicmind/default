let
  pkgs = import ../pkgs/stable.nix;
  builders.rubyGem = import ../builders/ruby-gem pkgs;
in
  pkgs.stdenv.mkDerivation (
        (import ../src/basic.nix)
    //  (rec {
          name = "builder";

          buildInputs = [
            pkgs.git
            pkgs.cacert
            pkgs.glibcLocales
            pkgs.ruby
            pkgs.rubyPackages.nokogiri
            pkgs.google-cloud-sdk
          ];

          rubyGemNanoc = builders.rubyGem "nanoc:4.11.14";
          rubyGemSlim = builders.rubyGem "slim:4.0.1";
          rubyGemAdsf = builders.rubyGem "adsf:1.4.3";
          rubyGemW3cvalidators = builders.rubyGem "w3c_validators:1.3.5";
          rubyGemAsciidoctor = builders.rubyGem "asciidoctor:2.0.10";
        })
  )
