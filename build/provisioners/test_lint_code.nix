let
  pkgs = import ../pkgs/stable.nix;
  builders.rubyGem = import ../builders/ruby-gem pkgs;
  builders.nodePackage = import ../builders/nodejs-module pkgs;
in
  pkgs.stdenv.mkDerivation (
        (import ../src/basic.nix)
    //  (rec {
          name = "builder";

          buildInputs = [
            pkgs.git
            pkgs.parallel
            pkgs.shellcheck
            pkgs.ruby
            pkgs.nodejs
          ];

          rubyGemSlimlint = builders.rubyGem "slim_lint:0.20.1";

          nodeJsModuleEslint =
            builders.nodePackage "eslint@7.1.0";
          nodeJsModuleEslintpluginfs =
            builders.nodePackage "eslint-plugin-fp@2.3.0";
          nodeJsModuleEslintstrict =
            builders.nodePackage "eslint-config-strict@14.0.1";
        })
  )
