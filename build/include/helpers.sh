# shellcheck shell=bash

function helper_use_pristine_workdir {
  export WORKDIR
  export STARTDIR

  function helper_teardown_workdir {
        echo "[INFO] Deleting: ${WORKDIR}" \
    &&  rm -rf "${WORKDIR}"
  }
  trap 'helper_teardown_workdir' 'EXIT'

      echo '[INFO] Creating a pristine workdir' \
  &&  rm -rf "${WORKDIR}" \
  &&  mkdir -p "${WORKDIR}" \
  &&  echo '[INFO] Copying files to workdir' \
  &&  cp -r "${STARTDIR}/." "${WORKDIR}" \
  &&  echo '[INFO] Entering the workdir' \
  &&  pushd "${WORKDIR}" \
  &&  echo '[INFO] Running: git clean -xdf' \
  &&  git clean -xdf \
  ||  return 1
}

function helper_docker_build_and_push {
  local tag="${1}"
  local context="${2}"
  local dockerfile="${3}"
  local build_arg_1_key="${4:-build_arg_1_key}"
  local build_arg_1_val="${5:-build_arg_1_val}"
  local build_arg_2_key="${6:-build_arg_2_key}"
  local build_arg_2_val="${7:-build_arg_2_val}"
  local build_arg_3_key="${8:-build_arg_3_key}"
  local build_arg_3_val="${9:-build_arg_3_val}"
  local build_arg_4_key="${10:-build_arg_4_key}"
  local build_arg_4_val="${11:-build_arg_4_val}"
  local build_arg_5_key="${12:-build_arg_5_key}"
  local build_arg_5_val="${13:-build_arg_5_val}"
  local build_arg_6_key="${14:-build_arg_6_key}"
  local build_arg_6_val="${15:-build_arg_6_val}"
  local build_arg_7_key="${16:-build_arg_7_key}"
  local build_arg_7_val="${17:-build_arg_7_val}"
  local build_arg_8_key="${18:-build_arg_8_key}"
  local build_arg_8_val="${19:-build_arg_8_val}"
  local build_arg_9_key="${20:-build_arg_9_key}"
  local build_arg_9_val="${21:-build_arg_9_val}"
  local build_args=(
    --tag "${tag}"
    --file "${dockerfile}"
    --build-arg "${build_arg_1_key}=${build_arg_1_val}"
    --build-arg "${build_arg_2_key}=${build_arg_2_val}"
    --build-arg "${build_arg_3_key}=${build_arg_3_val}"
    --build-arg "${build_arg_4_key}=${build_arg_4_val}"
    --build-arg "${build_arg_5_key}=${build_arg_5_val}"
    --build-arg "${build_arg_6_key}=${build_arg_6_val}"
    --build-arg "${build_arg_7_key}=${build_arg_7_val}"
    --build-arg "${build_arg_8_key}=${build_arg_8_val}"
    --build-arg "${build_arg_9_key}=${build_arg_9_val}"
  )

      echo "[INFO] Logging into: ${CI_REGISTRY}" \
  &&  docker login \
        --username "${CI_REGISTRY_USER}" \
        --password "${CI_REGISTRY_PASSWORD}" \
      "${CI_REGISTRY}" \
  &&  echo "[INFO] Pulling: ${tag}" \
  &&  if docker pull "${tag}"
      then
        build_args+=( --cache-from "${tag}" )
      fi \
  &&  echo "[INFO] Building: ${tag}" \
  &&  docker build "${build_args[@]}" "${context}" \
  &&  echo "[INFO] Pushing: ${tag}" \
  &&  docker push "${tag}" \
  &&  echo "[INFO] Deleting local copy of: ${tag}" \
  &&  docker image remove "${tag}"
}

function helper_list_declared_jobs {
  declare -F \
    | sed 's/declare -f //' \
    | grep -P '^job_[a-z_]+' \
    | sed 's/job_//' \
    | sort
}

function helper_list_touched_files {
  local path

  git show --format= --name-only HEAD | while read -r path
  do
    if test -e "${path}"
    then
      echo "${path}"
    fi
  done
}

function helper_file_exists {
  local path="${1}"

  if [ -f "${path}" ]
  then
    return 0
  else
        echo "[ERROR] ${path} does not exist" \
    &&  return 1
  fi
}

function helper_gcp_login {
      echo "${GCP_SVC_ACCOUNT}" | base64 -d > account.json \
  &&  gcloud auth activate-service-account --key-file=account.json
}

function helper_gcp_logout {
  gcloud auth revoke
}

function terraform_login {
  export TF_VAR_cloudflare_email
  export TF_VAR_cloudflare_token

      TF_VAR_cloudflare_email="${CLOUDFLARE_EMAIL}" \
  &&  TF_VAR_cloudflare_token="${CLOUDFLARE_TOKEN}" \
  &&  echo "${AUTONOMIC_TLS_KEY}" | base64 -d > autonomic.key \
  &&  echo "${AUTONOMIC_TLS_CERT}" | base64 -d > autonomic.crt
}

function helper_image_optimized {
  local path="${1}"

      helper_file_exists "${path}" \
  &&  if optipng -simulate "${path}" 2>&1 | tail -n2 | grep -q 'already optimized.'
      then
            return 0
      else
            echo "[ERROR] ${path} is not optimized" \
        &&  return 1
      fi
}

function helper_image_size {
  local path="${1}"
  local size_bytes="${2}"

      helper_file_exists "${path}" \
  &&  size_bytes="$(stat -c %s "${path}")" \
  &&  if [ "${size_bytes}" -le "${size_bytes}" ]
      then
            return 0
      else
            echo "[ERROR] ${path} size is over ${size_bytes} bytes" \
        &&  return 1
      fi
}

function helper_image_allowed_mime {
  local path="${1}"
  local allowed_mimes="png|gif"
  local image_mime

      helper_file_exists "${path}" \
  &&  image_mime="$(file --mime-type "${path}" | cut -d' ' -f2)" \
  &&  if echo "${image_mime}" | grep -qP "^image/(${allowed_mimes})$"
      then
            return 0
      else
            echo "[ERROR] ${path} mime type is ${image_mime}." \
        &&  echo "Allowed mimes: ${allowed_mimes}" \
        &&  return 1
      fi
}

function helper_test_blog_max_columns {
  local file="${1}"
  local max_columns="${2}"
  local regex

      helper_file_exists "${file}" \
  &&  regex=".{${max_columns},}" \
  &&  if ! grep -Pq "${regex}" "${file}"
      then
        return 0
      else
            grep -P "${regex}" "${file}" \
        &&  echo "[ERROR] ${file} must be wrapped at column ${max_columns}" \
        &&  return 1
      fi
}

function helper_test_blog_regex {
  local path="${1}"
  local regex="${2}"
  local error="${3}"

      helper_file_exists "${path}" \
  &&  if pcregrep -qMH "${regex}" "${path}"
      then
            return 0
      else
            echo "[ERROR] ${path}: ${error}" \
        &&  return 1
      fi
}

function helper_test_blog_others {
  local file="${1}"
  local tests=(
    'tag_category_valid'
    'tag_kind_exists'
    'tag_title_exists'
    'tag_subtitle_exists'
    'tag_description_exists'
    'tag_keywords_exists'
    'title_35_chars'
  )
  declare -A data=(
    [regex_tag_category_valid]='^category: (Clients|Candidates)$'
    [error_tag_category_valid]='Category tag must be either Clients or Candidates'
    [regex_tag_kind_exists]='(^kind: article)'
    [error_tag_kind_exists]='All blog articles must have kind: article'
    [regex_tag_title_exists]='(^title:)'
    [error_tag_title_exists]='All articles must have a title tag'
    [regex_tag_subtitle_exists]='(^subtitle:)'
    [error_tag_subtitle_exists]='All articles must have a subtitle tag'
    [regex_tag_description_exists]='(^description:)'
    [error_tag_description_exists]='All articles must have a description tag'
    [regex_tag_keywords_exists]='(^keywords:)'
    [error_tag_keywords_exists]='All articles must have a keywords tag'
    [regex_title_35_chars]='^title: .{0,35}$'
    [error_title_35_chars]='Titles can only be 35 characters long'
  )

      helper_file_exists "${file}" \
  &&  for test in "${tests[@]}"
      do
            helper_test_blog_regex \
              "${file}" \
              "${data[regex_${test}]}" \
              "${data[error_${test}]}" \
        ||  return 1
      done
}

function helper_blog_cover_exists {
  local path="${1}"

  helper_file_exists "${path}/cover.png"
}

function helper_build_nix_caches_parallel {
  local n_provisioners
  local n_provisioners_per_group
  local n_provisioners_remaining
  export lower_limit
  export upper_limit

      n_provisioners=$(find build/provisioners/ -type f | wc -l) \
  &&  n_provisioners_per_group=$(( n_provisioners/CI_NODE_TOTAL )) \
  &&  n_provisioners_remaining=$(( n_provisioners%CI_NODE_TOTAL )) \
  &&  if [ "${n_provisioners_remaining}" -gt '0' ]
      then
        n_provisioners_per_group=$(( n_provisioners_per_group+=1 ))
      fi \
  &&  lower_limit=$(( (CI_NODE_INDEX-1)*n_provisioners_per_group )) \
  &&  upper_limit=$(( CI_NODE_INDEX*n_provisioners_per_group-1 )) \
  &&  upper_limit=$((
        upper_limit > n_provisioners-1 ? n_provisioners-1 : upper_limit
      ))
}

function helper_lint_code_js {
  local path="${1}"
  local config="${2}"

  eslint \
    -c "${config}" \
    "${path}"
}

function helper_lint_code_slim {
  local path="${1}"
  local config="${2}"

  slim-lint \
    -c "${config}" \
    "${path}"
}

function helper_lint_code_shell {
  local path="${1}"

  shellcheck \
    --external-sources --exclude=SC1090,SC2154,SC2153 \
    "${path}"
}
