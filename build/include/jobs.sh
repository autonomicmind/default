# shellcheck shell=bash

source "${srcIncludeHelpers}"
source "${srcEnv}"

function job_build_nix_caches {
  local provisioners
  local dockerfile='build/Dockerfile'
  local context='.'

      helper_use_pristine_workdir \
  &&  provisioners=(./build/provisioners/*) \
  &&  helper_build_nix_caches_parallel \
  &&  for (( i="${lower_limit}";i<="${upper_limit}";i++ ))
      do
            provisioner=$(basename "${provisioners[${i}]}") \
        &&  provisioner="${provisioner%.*}" \
        &&  helper_docker_build_and_push \
              "${CI_REGISTRY_IMAGE}/nix:${provisioner}" \
              "${context}" \
              "${dockerfile}" \
              'PROVISIONER' "${provisioner}" \
        ||  return 1
      done
}

function job_test_lint_code {
  local touched_files
  local slim_files
  local js_files
  local shell_files
  local slim_lint_config='build/configs/slim-lint.yaml'
  local eslint_config='build/configs/eslint.json'
  export -f helper_lint_code_js
  export -f helper_lint_code_slim
  export -f helper_lint_code_shell

      helper_use_pristine_workdir \
  &&  env_prepare_node_modules \
  &&  env_prepare_ruby_modules \
  &&  touched_files="$(helper_list_touched_files)" \
  &&  slim_files="$(echo "${touched_files}" | grep -P '.slim$')" || true \
  &&  js_files="$(echo "${touched_files}" | grep -P '.js$')" || true \
  &&  shell_files="$(echo "${touched_files}" | grep -P '.sh$')" || true \
  &&  if test -n "${js_files}"
      then
        echo "${js_files}" | parallel \
          --halt-on-error now,fail=1 \
          "helper_lint_code_js {} ${eslint_config}"
      fi \
  &&  if test -n "${slim_files}"
      then
        echo "${slim_files}" | parallel \
          --halt-on-error now,fail=1 \
          "helper_lint_code_slim {} ${slim_lint_config}"
      fi \
  &&  if test -n "${shell_files}"
      then
        echo "${shell_files}" | parallel \
          --halt-on-error now,fail=1 \
          'helper_lint_code_shell {}'
      fi
}

function job_test_blog {
  local blogs
  local max_columns='81'

      blogs="$(find web/content/en/blog/ -name '*.adoc')" \
  &&  for blog in ${blogs}
      do
            helper_test_blog_max_columns "${blog}" "${max_columns}" \
        &&  helper_test_blog_others "${blog}" \
        ||  return 1
      done
}

function job_test_images {
  local images
  local png_images
  local blogs
  local max_image_size='1000000'
  local blog_cover_whitelist=(
    'web/content/en/blog/customers'
    'web/content/en/blog/candidates'
  )

      helper_use_pristine_workdir \
  &&  images="$(find web/ -name '*' -exec file --mime-type {} \; | grep -oP '.*(?=: image/)')" \
  &&  png_images="$(echo "${images}" | grep -oP '.+.png')" \
  &&  blogs="$(find web/content/en/blog/ -mindepth 1 -type d)" \
  &&  for image in ${images}
      do
            helper_image_size "${image}" "${max_image_size}" \
        &&  helper_image_allowed_mime "${image}" \
        ||  return 1
      done \
  &&  for image in ${png_images}
      do
            helper_image_optimized "${image}" \
        ||  return 1
      done \
  &&  for blog in ${blogs}
      do
            if [[ " ${blog_cover_whitelist[*]} " =~ ${blog} ]]
            then
              continue
            else
              helper_blog_cover_exists "${blog}"
            fi \
        ||  return 1
      done
}

function job_test_commit_msg {
  local commit_diff
  local commit_hashes

      helper_use_pristine_workdir \
  &&  env_prepare_node_modules \
  &&  git fetch --prune > /dev/null \
  &&  if [ "${IS_LOCAL_BUILD}" = "${TRUE}" ]
      then
            commit_diff="origin/master..${CI_COMMIT_REF_NAME}"
      else
            commit_diff="origin/master..origin/${CI_COMMIT_REF_NAME}"
      fi \
  &&  commit_hashes="$(git log --pretty=%h "${commit_diff}")" \
  &&  for commit_hash in ${commit_hashes}
      do
            git log -1 --pretty=%B "${commit_hash}" | commitlint \
        ||  return 1
      done
}

function job_pages_local {
      helper_use_pristine_workdir \
  &&  env_set_utf8_encoding \
  &&  env_prepare_ruby_modules \
  &&  pushd web/ || return 1 \
  &&  nanoc \
  &&  nanoc view
}

function job_test_infra {

  trap 'helper_gcp_logout' 'EXIT'

      helper_use_pristine_workdir \
  &&  pushd infra/ || return 1 \
  &&  helper_gcp_login \
  &&  terraform_login \
  &&  terraform init \
  &&  terraform plan -refresh=true  \
  &&  tflint \
  &&  popd || return 1
}

function job_deploy_infra {

  trap 'helper_gcp_logout' 'EXIT'

      helper_use_pristine_workdir \
  &&  pushd infra/ || return 1 \
  &&  helper_gcp_login \
  &&  terraform_login \
  &&  terraform init \
  &&  terraform apply -auto-approve -refresh=true \
  &&  popd || return 1
}

function job_deploy_pages {
  local web_bucket='autonomicmind-web'

  trap 'helper_gcp_logout' 'EXIT'

      helper_use_pristine_workdir \
  &&  env_set_utf8_encoding \
  &&  env_prepare_ruby_modules \
  &&  pushd web/ || return 1 \
  &&  nanoc \
  &&  helper_gcp_login \
  &&  gsutil -m rsync -d -r -a public-read output/ "gs://${web_bucket}/" \
  &&  popd || return 1
}

function job_reviews {
  reviews .reviews.toml
}
