# shellcheck shell=bash

# base directory with write permissions
mkdir root

# where repository files will be placed
mkdir root/src
mkdir root/src/repo

# where npm modules will be stored
mkdir root/nodejs
