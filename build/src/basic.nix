rec {
  srcEnv = ../include/env.sh;
  srcIncludeHelpers = ../include/helpers.sh;
  srcIncludeCli = ../include/cli.sh;
  srcIncludeGenericShellOptions = ../include/generic/shell-options.sh;
  srcIncludeGenericDirStructure = ../include/generic/dir-structure.sh;
  srcIncludeJobs = ../include/jobs.sh;
}
