variable "webBucketName" {
}

variable "mainDomain" {
}

resource "google_compute_backend_bucket" "autonomic-web" {
  name        = "web-bucket-backend"
  description = "Backend resource to serve the GCS content through an HTTP(S) Load Balancer"
  bucket_name = var.webBucketName
  enable_cdn  = false
}

resource "google_compute_url_map" "load-balancer-redirections" {
  name            = "web-redirections"
  description     = "Redirections rules applied by the Load Balancer"
  default_service = google_compute_backend_bucket.autonomic-web.self_link

  host_rule {
    hosts = [
      var.mainDomain,
    ]
    path_matcher = "allpaths"
  }

  path_matcher {
    name            = "allpaths"
    description     = "Redirect all requests to the bucket backend service"
    default_service = google_compute_backend_bucket.autonomic-web.self_link

    path_rule {
      paths   = ["/*"]
      service = google_compute_backend_bucket.autonomic-web.self_link
    }
  }

  test {
    service = google_compute_backend_bucket.autonomic-web.self_link
    host    = var.mainDomain
    path    = "/en"
  }
}

resource "google_compute_ssl_certificate" "autonomic_certificate" {
  name        = "autonomic-certificate"
  private_key = file("autonomic.key")
  certificate = file("autonomic.crt")

  lifecycle {
    create_before_destroy = true
  }
}

resource "google_compute_target_https_proxy" "https_load_balancer" {
  name             = "load-balancer"
  url_map          = google_compute_url_map.load-balancer-redirections.self_link
  ssl_certificates = [google_compute_ssl_certificate.autonomic_certificate.self_link]
}

resource "google_compute_global_address" "load_balancer_static_address" {
  name = "autonomic-static-address"
}

resource "google_compute_global_forwarding_rule" "forwarding_rule" {
  name       = "autonomic-web"
  target     = google_compute_target_https_proxy.https_load_balancer.self_link
  ip_address = google_compute_global_address.load_balancer_static_address.address
  port_range = "443"
}

resource "google_compute_health_check" "web-health-check" {
  name = "web-health-check"

  https_health_check {
    host         = var.mainDomain
    port         = 443
    request_path = "/en"
  }
}

output "ipAddress" {
  value = google_compute_global_address.load_balancer_static_address.address
}
