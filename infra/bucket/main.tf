variable "webBucketName" {
}

resource "google_storage_bucket" "autonomic-web" {
  name          = var.webBucketName
  location      = "US"
  storage_class = "MULTI_REGIONAL"

  labels = {
    company = "autonomic"
    task    = "website"
  }

  website {
    main_page_suffix = "index.html"
    not_found_page   = "error.html"
  }

  versioning {
    enabled = false
  }

  force_destroy = true
}

resource "google_storage_bucket_acl" "public_bucket" {
  bucket         = google_storage_bucket.autonomic-web.name
  predefined_acl = "publicRead"
}

output "bucketName" {
  value = google_storage_bucket.autonomic-web.name
}
