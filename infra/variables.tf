variable "mainDomain" {
  default = "autonomicmind.com"
}
variable "secDomain" {
  default = "autonomicmind.co"
}
variable "webBucketName" {
  default = "autonomicmind-web"
}
variable "cloudflare_email" {
}
variable "cloudflare_token" {
}
