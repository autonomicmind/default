variable "secDomain" {
}

resource "cloudflare_zone" "autonomicmind_co" {
  zone = var.secDomain
}

resource "cloudflare_zone_settings_override" "autonomicmind_co_settings" {
  name = cloudflare_zone.autonomicmind_co.zone

  settings {
    always_online            = "on"
    always_use_https         = "on"
    automatic_https_rewrites = "on"
    brotli                   = "on"
    browser_check            = "on"
    cache_level              = "basic"
    email_obfuscation        = "on"
    hotlink_protection       = "on"
    ip_geolocation           = "on"
    ipv6                     = "on"
    opportunistic_encryption = "on"
    min_tls_version          = "1.2"
    ssl                      = "full"

    minify {
      css  = "on"
      html = "on"
      js   = "on"
    }

    security_header {
      enabled = true
      max_age = 31536000
    }
  }
}

resource "cloudflare_record" "autonomicmind_co_web" {
  domain  = cloudflare_zone.autonomicmind_co.zone
  name    = cloudflare_zone.autonomicmind_co.zone
  type    = "A"
  value   = var.ipAddress
  proxied = true
}

resource "cloudflare_record" "autonomicmind_co_rebrandley" {
  domain  = cloudflare_zone.autonomicmind_co.zone
  name    = "go.${cloudflare_zone.autonomicmind_co.zone}"
  type    = "A"
  value   = "52.72.49.79"
  proxied = true
}

resource "cloudflare_record" "autonomicmind_co_wwww" {
  domain  = cloudflare_zone.autonomicmind_co.zone
  name    = "www.${cloudflare_zone.autonomicmind_co.zone}"
  type    = "CNAME"
  value   = cloudflare_zone.autonomicmind_co.zone
  proxied = true
}

resource "cloudflare_record" "autonomicmind_co_mailgun_cname" {
  domain = cloudflare_zone.autonomicmind_co.zone
  name   = "mail.mailgun.${cloudflare_zone.autonomicmind_co.zone}"
  type   = "CNAME"
  value  = "mailgun.org"
  ttl    = 300
}

resource "cloudflare_record" "autonomicmind_co_mailgun_mx_1" {
  domain = cloudflare_zone.autonomicmind_co.zone
  name   = "mailgun.${cloudflare_zone.autonomicmind_co.zone}"
  type   = "MX"
  priority = 10
  value  = "mxa.mailgun.org"
  ttl    = 300
}

resource "cloudflare_record" "autonomicmind_co_mailgun_cname_mx_2" {
  domain = cloudflare_zone.autonomicmind_co.zone
  name   = "mailgun.${cloudflare_zone.autonomicmind_co.zone}"
  type   = "MX"
  priority = 20
  value  = "mxb.mailgun.org"
  ttl    = 300
}

resource "cloudflare_record" "autonomicmind_co_email_1" {
  domain   = cloudflare_zone.autonomicmind_co.zone
  name     = cloudflare_zone.autonomicmind_co.zone
  type     = "MX"
  priority = 1
  value    = "aspmx.l.google.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicmind_co_email_2" {
  domain   = cloudflare_zone.autonomicmind_co.zone
  name     = cloudflare_zone.autonomicmind_co.zone
  type     = "MX"
  priority = 5
  value    = "alt1.aspmx.l.google.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicmind_co_email_3" {
  domain   = cloudflare_zone.autonomicmind_co.zone
  name     = cloudflare_zone.autonomicmind_co.zone
  type     = "MX"
  priority = 5
  value    = "alt2.aspmx.l.google.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicmind_co_email_4" {
  domain   = cloudflare_zone.autonomicmind_co.zone
  name     = cloudflare_zone.autonomicmind_co.zone
  type     = "MX"
  priority = 10
  value    = "aspmx2.googlemail.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicmind_co_email_5" {
  domain   = cloudflare_zone.autonomicmind_co.zone
  name     = cloudflare_zone.autonomicmind_co.zone
  type     = "MX"
  priority = 10
  value    = "aspmx3.googlemail.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicmind_co_verification" {
  domain = cloudflare_zone.autonomicmind_co.zone
  name   = cloudflare_zone.autonomicmind_co.zone
  type   = "TXT"
  value  = "google-site-verification=R2imN4uCL_EpqPkvjLSTOR0DLwRBVNkN2NZ2XGNQ3w8"
  ttl    = 300
}

resource "cloudflare_record" "autonomicmind_co_email_authentication" {
  domain = cloudflare_zone.autonomicmind_co.zone
  name   = "google._domainkey.${cloudflare_zone.autonomicmind_co.zone}"
  type   = "TXT"
  value  = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl7+W4gXSnC12v5bo5DaI8smO6L8XzKXRn53KQ0FsZxnlJ1o1AnECkAhozXiMrbSuLsizkhyBCuUD6BTNbo3j5/iYeGWzUAkxpc69e+iJyFGFIuQa0gZSkdflKE59WUbAxDv5JXzHNvGCoyA7GzvFG3wlhImS4qjlK6gquCqjt26Pab0tOFtCfcM4Nlqu+9d1YdXgrM2EdOBdinU9WIaEOOEt4h1T7kssyPt4pRtB1/FzqEjIOziyWqeh9KRTmAYCXHMsLQHT0dYTVzBBY/Z+98t+0mErDcZjU+9x6HXqkQvLPcyRn+r99YfaEhYcMumUMTGMQkPAzectG/fZDfHpRQIDAQAB"
  ttl    = 300
}

resource "cloudflare_record" "autonomicmind_co_mailgun_txt_1" {
  domain = cloudflare_zone.autonomicmind_co.zone
  name   = "krs._domainkey.mailgun.${cloudflare_zone.autonomicmind_co.zone}"
  type   = "TXT"
  value  = "\"k=rsa;\" \"p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCyvYuwXCt1yzOaiCcPHDT2x5JmNpxd4MJ74U2Dud+9MvLLeNBIZRLkKATpKXYhbon8LZZE3Iz9Imnse8bqbPSUlCbo2dRdG76IHoXfvDjYFaKiwZZDT6/Pjvh8477XWjhYXCIt2tBpf3sZwsqgkdS1XuFNMGa7LpLiC0a8mHTBWwIDAQAB\""
  ttl    = 300
}

resource "cloudflare_record" "autonomicmind_co_mailgun_txt_2" {
  domain = cloudflare_zone.autonomicmind_co.zone
  name   = "mailgun.${cloudflare_zone.autonomicmind_co.zone}"
  type   = "TXT"
  value  = "\"v=spf1\" \"include:mailgun.org\" \"~all\""
  ttl    = 300
}

resource "cloudflare_page_rule" "autonomicmind_co_redirect_top_domain" {
  zone     = cloudflare_zone.autonomicmind_co.zone
  target   = "${cloudflare_zone.autonomicmind_co.zone}*"
  status   = "active"
  priority = 1

  actions {
    forwarding_url {
      url         = "https://${cloudflare_zone.autonomicmind_com.zone}$1"
      status_code = 301
    }
  }
}

resource "cloudflare_page_rule" "autonomicmind_co_redirect_www" {
  zone     = cloudflare_zone.autonomicmind_co.zone
  target   = "www.${cloudflare_zone.autonomicmind_co.zone}*"
  status   = "active"
  priority = 2

  actions {
    forwarding_url {
      url         = "https://${cloudflare_zone.autonomicmind_com.zone}$1"
      status_code = 301
    }
  }
}
