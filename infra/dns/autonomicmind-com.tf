variable "mainDomain" {
}

variable "ipAddress" {
}

resource "cloudflare_zone" "autonomicmind_com" {
  zone = var.mainDomain
}

resource "cloudflare_zone_settings_override" "autonomicmind_com_settings" {
  name = cloudflare_zone.autonomicmind_com.zone

  settings {
    always_online            = "on"
    always_use_https         = "on"
    automatic_https_rewrites = "on"
    brotli                   = "on"
    browser_check            = "on"
    cache_level              = "basic"
    email_obfuscation        = "on"
    hotlink_protection       = "on"
    ip_geolocation           = "on"
    ipv6                     = "on"
    opportunistic_encryption = "on"
    min_tls_version          = "1.2"
    ssl                      = "full"

    minify {
      css  = "on"
      html = "on"
      js   = "on"
    }

    security_header {
      enabled = true
      max_age = 31536000
    }
  }
}

resource "cloudflare_record" "autonomicmind_com_web" {
  domain  = cloudflare_zone.autonomicmind_com.zone
  name    = cloudflare_zone.autonomicmind_com.zone
  type    = "A"
  value   = var.ipAddress
  proxied = true
}

resource "cloudflare_record" "autonomicmind_com_wwww" {
  domain  = cloudflare_zone.autonomicmind_com.zone
  name    = "www.${cloudflare_zone.autonomicmind_com.zone}"
  type    = "CNAME"
  value   = cloudflare_zone.autonomicmind_com.zone
  proxied = true
}

resource "cloudflare_record" "autonomicmind_com_checkly" {
  domain  = cloudflare_zone.autonomicmind_com.zone
  name    = "status.${cloudflare_zone.autonomicmind_com.zone}"
  type    = "CNAME"
  value   = "dashboards.checklyhq.com"
  proxied = false
}

resource "cloudflare_record" "autonomicmind_com_careers" {
  domain  = cloudflare_zone.autonomicmind_com.zone
  name    = "careers.${cloudflare_zone.autonomicmind_com.zone}"
  type    = "CNAME"
  value   = "recruit.cs.zohohost.com"
  proxied = true
}

resource "cloudflare_record" "autonomicmind_com_careers_zr" {
  domain  = cloudflare_zone.autonomicmind_com.zone
  name    = "ZRd9d8ac627820fadd1667d9c06fa8aae2.${cloudflare_zone.autonomicmind_com.zone}"
  type    = "CNAME"
  value   = "recruit.cs.zohohost.com"
  proxied = true
}

resource "cloudflare_record" "autonomicmind_com_email_1" {
  domain   = cloudflare_zone.autonomicmind_com.zone
  name     = cloudflare_zone.autonomicmind_com.zone
  type     = "MX"
  priority = 1
  value    = "aspmx.l.google.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicmind_com_email_2" {
  domain   = cloudflare_zone.autonomicmind_com.zone
  name     = cloudflare_zone.autonomicmind_com.zone
  type     = "MX"
  priority = 5
  value    = "alt1.aspmx.l.google.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicmind_com_email_3" {
  domain   = cloudflare_zone.autonomicmind_com.zone
  name     = cloudflare_zone.autonomicmind_com.zone
  type     = "MX"
  priority = 5
  value    = "alt2.aspmx.l.google.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicmind_com_email_4" {
  domain   = cloudflare_zone.autonomicmind_com.zone
  name     = cloudflare_zone.autonomicmind_com.zone
  type     = "MX"
  priority = 10
  value    = "aspmx2.googlemail.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicmind_com_email_5" {
  domain   = cloudflare_zone.autonomicmind_com.zone
  name     = cloudflare_zone.autonomicmind_com.zone
  type     = "MX"
  priority = 10
  value    = "aspmx3.googlemail.com"
  ttl      = 300
}

resource "cloudflare_record" "autonomicmind_com_verification" {
  domain = cloudflare_zone.autonomicmind_com.zone
  name   = cloudflare_zone.autonomicmind_com.zone
  type   = "TXT"
  value  = "google-site-verification=VBHKijD04F31i9aEltUWEBbFUhtsvaNbtXozm3Ss31U"
  ttl    = 300
}

resource "cloudflare_record" "autonomicmind_com_email_authentication" {
  domain = cloudflare_zone.autonomicmind_com.zone
  name   = "google._domainkey.${cloudflare_zone.autonomicmind_com.zone}"
  type   = "TXT"
  value  = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhfFuh/eyFUJjqrOAlEEKQn8i+v8Cdu4zYcOROxbyFcr7gPBidkoh+MQPyrrCSJWVBISwxYXTqqs/+Uag+W/vBhTQWrioao7DDUPBGaxeiW1xehLbYc0LkkMeRp3dNy78CqWYkjn16It6gGLLNQzEkZTwSHkBBHVszY5dWyuhjWPU8Uv1itfkVIllTf7uNzrtogTXToinPj9icskTX+srRHvYuiu9dY8EGzI5ttxNytgvmvqNlsdBOD+3RCmkaA2Peph2ovRmG6XjopfvJWOKeCTGK+IJKAAVA+G9i8KaY321HqixVx6eo7jgLafvbr+hpGbSbaoqqagquRGTjs59eQIDAQAB"
  ttl    = 300
}

resource "cloudflare_record" "autonomicmind_com_mandrill_spf" {
  domain = cloudflare_zone.autonomicmind_com.zone
  name   = cloudflare_zone.autonomicmind_com.zone
  type   = "TXT"
  value  = "v=spf1 include:spf.mandrillapp.com ?all"
  ttl    = 300
}

resource "cloudflare_record" "autonomicmind_com_mandrill_authentication" {
  domain = cloudflare_zone.autonomicmind_com.zone
  name   = "mandrill._domainkey.${cloudflare_zone.autonomicmind_com.zone}"
  type   = "TXT"
  value  = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCrLHiExVd55zd/IQ/J/mRwSRMAocV/hMB3jXwaHH36d9NaVynQFYV8NaWi69c1veUtRzGt7yAioXqLj7Z4TeEUoOLgrKsn8YnckGs9i3B3tVFB+Ch/4mPhXWiNfNdynHWBcPcbJ8kjEQ2U8y78dHZj1YeRXXVvWob2OaKynO8/lQIDAQAB"
  ttl    = 300
}

resource "cloudflare_page_rule" "autonomicmind_com_redirect_www" {
  zone     = cloudflare_zone.autonomicmind_com.zone
  target   = "www.${cloudflare_zone.autonomicmind_com.zone}*"
  status   = "active"
  priority = 1

  actions {
    forwarding_url {
      url         = "https://${cloudflare_zone.autonomicmind_com.zone}$1"
      status_code = 301
    }
  }
}

resource "cloudflare_page_rule" "autonomicmind_com_redirect_forms" {
  zone     = cloudflare_zone.autonomicmind_com.zone
  target   = "${cloudflare_zone.autonomicmind_com.zone}/forms*"
  status   = "active"
  priority = 2

  actions {
    forwarding_url {
      url         = "https://fluidattacks.formstack.com/forms$1"
      status_code = 301
    }
  }
}

resource "cloudflare_page_rule" "autonomicmind_com_redirect_challenges" {
  zone     = cloudflare_zone.autonomicmind_com.zone
  target   = "${cloudflare_zone.autonomicmind_com.zone}/challenges*"
  status   = "active"
  priority = 3

  actions {
    forwarding_url {
      url         = "https://autonomicjump.gitlab.io/challenges$1"
      status_code = 301
    }
  }
}
