data "null_data_source" "auth_anyone_allowed" {
  inputs = {
    name  = "anyone"
    value = "0.0.0.0/0"
  }
}

resource "google_sql_database_instance" "courses-db" {
  name             = "courses-db"
  database_version = "MYSQL_5_6"
  region           = "us-central1"

  settings {
    tier = "db-g1-small"
    ip_configuration {
      ipv4_enabled = true
      dynamic "authorized_networks" {
        for_each = [data.null_data_source.auth_anyone_allowed.outputs]
        content {
          expiration_time = lookup(authorized_networks.value, "expiration_time", null)
          name            = lookup(authorized_networks.value, "name", null)
          value           = authorized_networks.value.value
        }
      }
    }
    backup_configuration {
      enabled    = true
      start_time = "01:00"
    }
  }
}
