terraform {
  backend "gcs" {
    bucket  = "autonomic-tf"
    credentials = "account.json"
    path = "terraform.tfstate"
  }
}

provider "google" {
  credentials = file("account.json")
  project     = "autonomic-web"
  region      = "us-east1"
}

provider "cloudflare" {
  email   = var.cloudflare_email
  token   = var.cloudflare_token
  version = "~> 1.18.1"
}

module "website" {
  source        = "./bucket"
  webBucketName = var.webBucketName
}

module "load-balancer" {
  source        = "./load-balancer"
  webBucketName = module.website.bucketName
  mainDomain    = var.mainDomain
}

module "sql" {
  source = "./sql"
}

module "dns" {
  source     = "./dns"
  ipAddress  = module.load-balancer.ipAddress
  mainDomain = var.mainDomain
  secDomain  = var.secDomain
}
