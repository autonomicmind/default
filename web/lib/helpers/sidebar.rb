module Sidebar
  def get_titles_and_ids(item)
    headers = Array.new
    ids = Array.new
    paragraphs = Array.new
    cont = 0
    start = false
    par = ""
    File.foreach(item) do |line|
      if (line[/^==\s[A-Z0-9]/])
        start = true
        header = line.to_s[3..-1] 
        id = '_' + header.to_s.downcase.gsub(' ', '_').tr('.', '')
        ids.push(id)
        headers.push(header)
        if cont != 0
          paragraphs.push(par)
          par = ""
        end
        cont += 1
      elsif start
        par += line + "\n"
      end
    end
    paragraphs.push(par)
    return headers, ids, paragraphs
  end
end
