module NanocRedirector
  module RedirectFrom
    def self.process(item, dest)
      return if item[:redirect_from].nil?
      return if dest.nil?
      redirect_hash = {}

      key = item.identifier.without_ext
      value = item[:redirect_from].is_a?(String) ? [item[:redirect_from]] : item[:redirect_from]

      redirect_hash[key] = value

      redirect_hash.values.each do |redirects|
        redirects.each do |redirect|
          content = NanocRedirector.redirect_template(dest)
          dir = File.join("output", redirect)
          redirect_path = File.join(dir, "index.html")
          FileUtils.mkdir_p(dir) unless File.directory?(dir)
          File.write(redirect_path, content) unless File.exist? redirect_path
        end
      end
    end
  end

  def self.redirect_template(item_url)
    <<-EOF
<!DOCTYPE html>
<html>
<head>
<title>Home - Redirection</title>
<meta charset=utf-8>
<link rel=canonical href="#{item_url}">
<meta http-equiv=refresh content="0; url=#{item_url}">
</body>
</html>
EOF
  end
end