---
title: Functional Business Analyst
subtitle:
description: >
  El objetivo principal de esta página es informar a talentos potenciales
  y a gente interesada en trabajar sobre nosotros
  acerca de nuestro proceso de selección.
  Aquí describimos los perfiles deseados
  y candidaturas abiertas de trabajo.
keywords:
  - 'Autonomic Mind'
  - 'Carreras'
  - 'Posición'
  - 'Perfil'
  - 'Selección'
  - 'Proceso'
  - 'Full Stack'
banner_title: Functional Business Analyst
lang: es
---

[.item]
==  Functional Business Analyst

Should be able to lead the business requirements to the development
and quality assurance team members. We are looking for a Functional
Business analyst with proficient English skills to be able to
understand and transad the QA members and ensure the quality of the solution.

* *Required Skills:*

** Fluent English (B2 or C1)
** 4 to 5 years of experience as business analyst
** Analysis, model and classification of technical requirements
** Definition of tests for compliance with the requirements
in the deliverables.
** Identify potential risks and take preventive action
** Knowledge in agile methodologies
** Knowledge in testing execution and monitoring tools

* *Details:*

** *Level of education required:* Professional
sistemas o afines.
** *Titles in the areas of:* Systems engineering or related
** *Professional Card:* No
** *Required certifications:* No
** *Time of experience in activities related to the vacancy:* 4 to 5 years
** *City of the vacancy:* Medellin or Remote in Colombia
** *Hours per week to work:* 45
** *Availability 7x24:* No
** *Date on which work is required to begin:* As soon as possible
** *Salary range of the vacancy:* $6.800.000 up to $7.250.000 COP monthly
(Depending on the english level )
** *Type of expected contract:* Indefinite
** *Requires fluent English:* Indefinite
** *Requires US VISA:* Indefinite

¿Estás interesado en esta vacante?
https://zfrmz.com/jLlKfvhrnZwyECQ3EgJI[*Aplica aquí*^,role=orange].
