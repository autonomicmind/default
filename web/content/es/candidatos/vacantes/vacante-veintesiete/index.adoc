---
title: Desarrollador Full Stack .Net
subtitle:
description: >
  El objetivo principal de esta página es informar a talentos potenciales
  y a gente interesada en trabajar sobre nosotros
  acerca de nuestro proceso de selección.
  Aquí describimos los perfiles deseados
  y candidaturas abiertas de trabajo.
keywords:
  - 'Autonomic Mind'
  - 'Carreras'
  - 'Posición'
  - 'Perfil'
  - 'Selección'
  - 'Proceso'
  - 'Full Stack'
banner_title: Desarrollador Full Stack .Net
lang: es
---

[.item]
== Desarrollador Full Stack .Net

Analizar, desarrollar e implementar productos de software
de excelente calidad, para brindar soluciones que cumplan
las expectativas del cliente interno y den respuesta a
las necesidades y exigencias del cliente externo.


* *Actividades del cargo:*

** Analizar y participar en el diseño de las funcionalidades
a desarrollar con el fin de implementarlas de forma eficiente
y eficaz.
** Garantizar la calidad en el comportamiento de las funcionalidades
implementadas con el fin de asegurar un desempeño adecuado.
** Seguir los lineamientos definidos por los arquitectos de la empresa
a fin de proponer modificaciones o mejoras que beneficien al cliente final.
** Realizar visitas a los clientes, acompañados de los Product Manager,
con el objetivo de conocer y caracterizar las necesidades en cuanto a
los productos actuales o a ** desarrollos futuros.
** Identificar y construir pruebas de desarrollo automatizadas y
sostenerlas en el tiempo.
** Promover la investigación en tecnologías y herramientas que ayuden
a la evolución del equipo.
** Gestionar y planear con IT la puesta en producción, actualización y
mantenimiento de los productos,mejoras y/o cambios desarrollados.
** Brindar soporte a las funcionalidades implementadas de los productos
desarrollados en caso de que lo amerite.


* *Habilidades necesarias:*

** Desarrollo y modelamiento de Bases de Datos SQL.
** Desarrollo de Software con tecnología .NET
** C#
** Angular 4 o superior
** Aplicaciones web ASP.NET (Web Forms, MVC)
** Servicios Web (WCF, REST, ASMX)

* *Conocimientos en:*

** Programación en SQL Server (T-SQL)
** Herramientas de automatización de pruebas y de despliegue
** Herramientas de control de código y gestión de la configuración
** Buenas prácticas de desarrollo (Análisis estático de código, prácticas propias de .NET)

* *Habilidades deseadas:*

** Tuning de Bases de Datos
** Administración de Bases de Datos
** Azure PAAS
** Cliente MVC - HTML5/CSS3/Angular JS
** Angular Material
** Google Maps API
** TDD, ATDD, DD, BDD, Spec Flow, Seleniun, MS Test
** Metodologías ágiles y principios de agilisimo: integración continua, xp, scrum

* *Detalles de la vacante:*

** *Nivel de escolaridad requerido:* tecnólogo o profesional en Sistemas o afines.
** *Tarjeta Profesional:* No
** *¿Acepta candidatos con alguna condición de discapacidad?* Si
** *Certificaciones requeridas:*  No
** *Años de experiencia en actividades afines a la vacante:* + de 3 años
** *Ciudad de la vacante:* Remoto
** *Horas por semana que debe trabajar:* 48
** *Disponibilidad 7x24:* No
** *Fecha en la que se requiere iniciar a trabajar:* Inmediato
** *Requiere hablar Inglés fluido:* No
** *Requiere VISA:* No
** *Rango salarial:* $6.000.000 COP - $7.000.000 COP
** *Tipo de contrato:* Indefinido

* *¿Qué ofrecemos?*

** Bonificación de contratación
** Bonificación de fin de año
** Plataforma de puntos
** Tiquetera de tiempo

¿Estás interesado en esta vacante?
https://zfrmz.com/jLlKfvhrnZwyECQ3EgJI[*Aplica aquí*^,role=orange].
