---
title: Python Developer Junior
subtitle:
description: >
  El objetivo principal de esta página es informar a talentos potenciales
  y a gente interesada en trabajar sobre nosotros
  acerca de nuestro proceso de selección.
  Aquí describimos los perfiles deseados
  y candidaturas abiertas de trabajo.
keywords:
  - 'Autonomic Mind'
  - 'Carreras'
  - 'Posición'
  - 'Perfil'
  - 'Selección'
  - 'Proceso'
  - 'Full Stack'
banner_title: Python Developer Junior
lang: es
---

[.item]
== Python Developer Junior

This entry-level position will provide an opportunity to expand and deepen one's
software engineering skills while contributing to a world-class language-learning
platform supporting over 1 million students in online education.  You will work along
with the Software Development Supervisor to help him  grow the technical and professional
skills of the software development team members, by keeping track of what are their strong
and weak points, and help plan their assignments and training, so that it will benefit the
product roadmap, the team and their personal and professional interests.

You also will have the chance to learn about modern web application technologies
and processes, learn Ruby on Rails and AWS, and be mentored on development best
practices within an agile, test-driven team.

* *Activities:*

** Help the software developers achieve their goals and sharpen their skills.
** Learn about modern software development practices like Test Driven Development,
Behavior Driven Development, Pair Programing, Refactoring, etc.
** Use code analysis tools like CodeClimate to help detect weak points in the code
quality and help create plans to fix them.
** Contribute as part of an Agile Scrum process.
** Create documents and media to help new developers reduce the time it takes them to
learn our internal processes.
** Develop scripts and applications to automate internal business workflows.
** Participate in pair-programming and code review exercises within the engineering team.


* *Required Skills:*

** Associate's degree or equivalent in computer science
** Experience with agile development practices (Scrum, Extreme programming,
Kanban, Timeboxing, Retrospective, etc.)
** 1-2 year's experience with an interpreted or object-oriented programming
language (Python, Ruby, etc.)
** Working knowledge of computer networks and core internet technologies
(DNS, HTTP/S, HTML, JS, CSS)
** Familiarity with databases, SQL, and object modeling
** Familiarity with pair-programming and code review processes
** Comfort with a command line terminal
** Working knowledge of git and git workflows
** Good communication skills (written and verbal)
** Eagerness to jump-in to new problems and learn new technologies

* *Details:*

** *Level of education required*: Information systems technologist or recently
graduated computer engineer.
** *Years of experience in activities related to the vacancy*: 1 year experience
in object-oriented programming languages and knowledge in relational bases.
** *City of the vacancy*: Medellín
** *Hours per week to work*: 48
** *Availability 7x24*: No
** *Requires fluent English*: B1
** *Salary range*:3.500.000
** *Type of contract*: Indefinite

* *What do we offer?*

** Indefinite contract
** Prepaid Medicine or 100% Employee Policy
** Work day 2x3
** Travel to Boston for training
** Training aids
** Learning of state-of-the-art development processes.





¿Estás interesado en esta vacante?
https://zfrmz.com/jLlKfvhrnZwyECQ3EgJI[*Aplica aquí*^,role=orange].
