---
title: Analista de Ciberseguridad
subtitle:
description: >
  El objetivo principal de esta página es informar a talentos potenciales
  y a gente interesada en trabajar sobre nosotros
  acerca de nuestro proceso de selección.
  Aquí describimos los perfiles deseados
  y candidaturas abiertas de trabajo.
keywords:
  - 'Autonomic Mind'
  - 'Carreras'
  - 'Posición'
  - 'Perfil'
  - 'Selección'
  - 'Proceso'
  - 'Full Stack'
banner_title: Analista de Ciberseguridad
lang: es
---

[.item]
==  Analista de Ciberseguridad

Buscamos profesionales en Ingeniería de sistemas o afines con experiencia
en la gestión e implementación de proyectos de ciberseguridad, con
conocimientos en el manejo de elementos en la nube AWS.

* *Actividades:*

** Garantizar la seguridad de la información identificando los
riesgos cibernéticos que se pueda enfrentar.
** Definir las metodologías en los equipos de trabajo para evolucionar
la seguridad de los proyectos a medida que el producto crece.
** Diseñar la arquitectura del producto que ayude a incrementar el
nivel de detalle de los controles de seguridad cuando sea requerido
para prevenir o mitigar riesgos de ciberseguridad.
** Modelar con las áreas involucradas las pruebas de seguridad
necesarias para validar la correcta implementación de los controles
diseñados en el marco de los proyectos de transformación.
Revisar y aceptar cuando se cumplen los requerimientos, los
resultados de las pruebas de seguridad realizadas.
** Identificar los impactos en las áreas de seguridad a nivel
corporativo de los proyectos para garantizar la continuidad de los
controles de seguridad en el día a día y colocar en operación
los nuevos controles.

* *Habilidades necesarias:*

** Ciberseguridad y seguridad de la Información
** Gestión de riesgos de ciberseguridad
** Arquitectura e infraestructura tecnológica, diseño y
desarrollo de aplicaciones
** Experiencia en gestión de proyectos de seguridad
** Conocimientos en Estándares de Seguridad Informática
(ISO/IEC 27001, 27002, 27005)
** Conocimientos en infraestructura en nube AWS


* *Habilidades deseadas:*

** Certificación de AWS Secure.
** Conocimientos en CloudFormation.
** Alta capacidad de aprendizaje.
** Liderazgo personal, autogestión, proactividad.
** Lenguajes de programación.

* *Detalles de la vacante:*

** *Nivel de escolaridad requerido:* Profesional en Ingeniería de
sistemas o afines.
** *Tiempo de experiencia en actividades afines a la vacante:* 3 a 5 años
en ciberseguridad
** *Ciudad de la vacante:* Medellín (2 días oficina - 3 home office)
** *Horas por semana que debe trabajar:* 48
** *Disponibilidad 7x24:* No
** *Tipo de contrato esperado:* Término indefinido
** *Rango salarial de la vacante:* $5.000.000 a $8.000.000 según
conocimientos y experiencia
** *Requiere hablar inglés fluido:* No
** *Requiere VISA de EEUU:* No

¿Estás interesado en esta vacante?
https://zfrmz.com/jLlKfvhrnZwyECQ3EgJI[*Aplica aquí*^,role=orange].
