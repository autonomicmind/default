---
title: Analista De Pruebas
subtitle:
description: >
  El objetivo principal de esta página es informar a talentos potenciales
  y a gente interesada en trabajar sobre nosotros
  acerca de nuestro proceso de selección.
  Aquí describimos los perfiles deseados
  y candidaturas abiertas de trabajo.
keywords:
  - 'Autonomic Mind'
  - 'Carreras'
  - 'Posición'
  - 'Perfil'
  - 'Selección'
  - 'Proceso'
  - 'Full Stack'
banner_title: Analista De Pruebas
lang: es
---

[.item]
== Analista De Pruebas

* *Habilidades necesarias*:


** Pruebas de Software / Quality Control
** Transact-SQL (Dominio de consultas / queries)
** Desarrollo - POO (Java, C#, .Net, Kotlin, Python)
** Prácticas de programación orientada a objetos
** Conocimiento de backend y APIs (SOA, Servicios
(Soap, Rest, Restfull) y Microservicios)
** Conocimiento en Servicios (REST, SOAP u Otro)
** Modelos de desarrollo de Software (Tradicionales,
ágiles, escalados, híbrido)
** Conceptos Agilismo - Scrum
** Experiencia Agilismo - Scrum
** Control de versiones (Git, Git Lab, BitBucket) - Git flow


* *Habilidades deseadas:*

** TDD
** Experiencia Automatización: Screenplay o Page Object Model
** Experiencia Automatización: Robot Framework, Nightwatch, Otros
(SerenityJS with Protractor (Angular), Cypress (React, Vue, otros JS))
** Conocimiento de desarrollo continuo
** Conocimiento de integración continua
** Conocimiento de despliegue continuo
** Conceptos en DevOps
** Gestión de proyectos


* *Detalles de la vacante:*

** *Nivel de escolaridad requerido*: Estudiante/ Técnico/ Tecnólogo/ Profesional
** *Años de experiencia en actividades afines a la vacante*: +1
** *Ciudad de la vacante*: Presencial Bogotá / Medellín
** *Horas por semana que debe trabajar*: 48
** *Requiere hablar Inglés fluido*: B1 (Técnico) - Avanzado es un plus
** *Rango salarial*: $3.500.000 - $4.200.000
** *Tipo de contrato*: Indefinido

* *¿Qué ofrecemos?*

** Seguro de vida
** Póliza de salud.


¿Estás interesado en esta vacante?
https://zfrmz.com/jLlKfvhrnZwyECQ3EgJI[*Aplica aquí*^,role=orange].
