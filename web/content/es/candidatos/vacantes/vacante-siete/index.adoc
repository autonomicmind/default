---
title: Ethical Hacker
subtitle:
description: >
  El objetivo principal de esta página es informar a talentos potenciales
  y a gente interesada en trabajar sobre nosotros
  acerca de nuestro proceso de selección.
  Aquí describimos los perfiles deseados
  y candidaturas abiertas de trabajo.
keywords:
  - 'Autonomic Mind'
  - 'Carreras'
  - 'Posición'
  - 'Perfil'
  - 'Selección'
  - 'Proceso'
  - 'Full Stack'
banner_title: Ethical Hacker
lang: es
---

[.item]
== Ethical Hacker


Whenever we have projects they are about hacking applications or
company networks, if we do not have projects we do software development
of our own tools. That's why we are looking for hackers who are passionate
about development and programming.

If you do not master them it does not matter, in our company what matters
is your ability to learn autonomously about the many challenges that we face
every day, the discipline to face them, the honesty to say what needs to be
said and collaboration without pretending to be more than anyone.

If you want to participate in the most challenging selection process
that exists, and from which whatever happens you will learn more than in
any other process, we invite you to get to know it and see for yourself
that in our company doing is worthier than knowing.

Our selection process is guided by Git, and it is fed back through a
continuous integrator and it also allows you to raise your world ranking
in programming and hacking.

You don't have to be a graduate, you don't have to be from any
particular career, or have specific experience. The only thing
that matters is that you show that you can learn autonomously at the
expected speed.

If you like programming and your passion is information security,
apply to our selection process. We are waiting for you.

* *Desired Skills:*

** Linux
** Python
** Burp
** Metasploit

* *Details:*

** *Salary:* $1.800.000 to 14.800.000 COP
** *Specific Experience:* 0 a 15 Years
** *City:* 100% remote
** *Type of expected contract:* Indefinite
** *English:* Technical Reading
** *Do you accept candidates with a disability condition:* Yes

¿Estás interesado en esta vacante?
https://zfrmz.com/jLlKfvhrnZwyECQ3EgJI[*Aplica aquí*^,role=orange].
