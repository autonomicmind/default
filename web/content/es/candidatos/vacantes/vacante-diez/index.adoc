---
title: Ejecutivo Comercial Senior
subtitle:
description: >
  El objetivo principal de esta página es informar a talentos potenciales
  y a gente interesada en trabajar sobre nosotros
  acerca de nuestro proceso de selección.
  Aquí describimos los perfiles deseados
  y candidaturas abiertas de trabajo.
keywords:
  - 'Autonomic Mind'
  - 'Carreras'
  - 'Posición'
  - 'Perfil'
  - 'Selección'
  - 'Proceso'
  - 'Full Stack'
banner_title: Ejecutivo Comercial Senior
lang: es
---

[.item]
== Ejecutivo Comercial Senior

Se requiere una persona con gran conocimiento del mercado mexicano,
que entienda sus dinámicas, conozca las necesidades empresariales
preferiblemente tecnológicas.

* *Actividades del Cargo:*

** Entender las necesidades del cliente y garantizar los requisitos del mismo.
** Velar por la satisfacción del cliente garantizando el cumplimiento de los
acuerdos establecidos.
** Relacionamiento con diferentes clientes.
** Negociar contratos y cerrar acuerdos para maximizar los beneficios.
** Desarrollar nuevos negocios con clientes nuevos y existentes para cumplir
con los objetivos de ventas.
** Negociar y mantener relaciones sólidas y duraderas con los clientes con
cuentas clave.
** Asegurar la entrega oportuna y exitosa de soluciones para cumplir
con las expectativas del cliente.
** Pronosticar y realizar seguimiento de las métricas claves de la cuenta.
** Actualizar plataformas y CRM de la empresa.
** Asistencia a eventos nacionales e internacionales.

* *Habilidades necesarias:*

** Comunicarse claramente, tanto de manera escrita como oral.
** Alto conocimiento en ventas.
** Conocimientos en métricas comerciales.
** Capacidad analítica para tomar decisiones.
** Capacidad para construir relaciones comerciales desde cero.
** Experiencia en el área de sistemas o industrias afines.
** Conocimiento en plataformas CRM.
** Manejo de inglés B2-C1.

* *Habilidades deseadas:*

** Experiencia en mercados extranjeros.

* *Detalles de la vacante:*

** *Nivel de escolaridad requerido:* Profesional en áreas administrativas
o comerciales.
** *Tarjeta Profesional:* No
** *Tiempo de experiencia en actividades afines a la vacante:* +4 años en
actividades relacionadas a ventas.
** *Ciudad de la vacante:* México
** *Horas por semana que debe trabajar:* 48
** *Disponibilidad 7x24:* No.
** *Fecha en la que se requiere iniciar a trabajar:* Inmediato
** *Rango salarial de la vacante:* Salario desde 2.500 USD hasta 4.000 USD
** *Tipo de contrato esperado:* Término indefinido
** *Requiere hablar inglés fluido:* B2 - C1
** *Requiere VISA de EEUU:* Si
** *Disponibilidad para viajar:* Si

¿Estás interesado en esta vacante?
https://zfrmz.com/jLlKfvhrnZwyECQ3EgJI[*Aplica aquí*^,role=orange].
