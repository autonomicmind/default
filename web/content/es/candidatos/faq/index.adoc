---
title: Preguntas frecuentes
subtitle: 
description: >
  Página hecho para que los aplicantes al proceso encuentren respuestas a
  las preguntas más comunes
keywords:
  - 'faqs'
  - 'preguntas'
  - 'respuestas'
  - 'dudas'
  - 'pensamientos'
banner_title: Preguntas frecuentes\n (FAQ)
lang: es
---

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_1"]
¿Por qué si ya apliqué en un portal de empleo, no me invitan al proceso?
[role="faq_p w-80 w-90-l ma-auto dn faq_1_content"]
Una vez que apliques a una de nuestras vacantes
en alguno de los portales en los cuales estamos registrados,
nos tomamos de +1+ a +2+ días en identificar candidatos potenciales,
clasificar e invitar vía e-mail.
Si por algún motivo no has sido invitado
pasados +2+ días (excluyendo fines de semana)
y consideras que tu perfil encaja con lo descrito en la vacante
puedes escribirnos a [am-orange b]#careers@autonomicmind.com#.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_2"]
La aplicación del examen no me deja salir de Moodle, ¿qué puedo hacer?
[role="faq_p w-80 w-90-l ma-auto dn faq_2_content"]
Si ya finalizaste el examen,
da click en el botón que está situado
en la parte inferior derecha de la pantalla para salir.
Si esto no funciona, puedes intentar reiniciando el equipo con tranquilidad,
pues ya para este momento habrás enviado el examen
y no se perderá ningún dato.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_3"]
No sé para cuál vacante me postulé, ¿cómo puedo averiguarlo?
[role="faq_p w-80 w-90-l ma-auto dn faq_3_content"]
Escribir cualquier duda u observación
respecto al proceso de selección al correo [am-orange b]#careers@autonomicmind.com#.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_4"]
Ya me había postulado para una vacante, ¿lo puedo hacer de nuevo para otro
perfil o para el mismo?
[role="faq_p w-80 w-90-l ma-auto dn faq_4_content"]
Sí, lo puedes hacer, y en caso de haber pasado
más de seis meses desde tu última postulación,
debes hacer el proceso de nuevo desde el principio.
En caso de no haber pasado los seis meses,
nos escribes un correo a [am-orange b]#careers@autonomicmind.com#
informándonos que quieres aplicar a otro perfil.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_5"]
¿Me puedo postular para dos vacantes diferentes?
[role="faq_p w-80 w-90-l ma-auto dn faq_5_content"]
Sí, haces el proceso una sola vez y nos escribes
a cuáles vacantes deseas aplicar al correo [am-orange b]#careers@autonomicmind.com#

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_6"]
¿Qué experiencia se requiere para aplicar a las vacantes?
[role="faq_p w-80 w-90-l ma-auto dn faq_6_content"]
En la sección de [button badge]#link:../vacantes/[vacantes]#
puedes identificar la experiencia requerida para cada vacante.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_7"]
No me funciona la aplicación de examen, ¿qué puedo hacer?
[role="faq_p w-80 w-90-l ma-auto dn faq_7_content"]
Una vez descargues e instales la aplicación SafeExamBrowser,
no la abras directamente.
En vez de eso,
ejecuta el archivo “fluidexams.seb”,
que se encuentra en el archivo que descargaste.
Si el problema persiste, intenta:

[role="am-gray f5 f4-l w-80 w-90-l ma-auto dn faq_7_content ws"]
** Abrir el archivo como administrador
** Intentar en otro PC, bien sea en un café internet
** En caso de que no cuentes con sistema operativo Windows,
puedes descargar la aplicación en el siguiente [button badge]#link:https://safeexambrowser.org/download_en.html[enlace]#

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_8"]
Se me pasó el tiempo para el examen, ¿qué puedo hacer?
[role="faq_p w-80 w-90-l ma-auto dn faq_8_content"]
De tener un motivo justificado
(bloqueos de la aplicación, reinicios inesperados, problemas de conexión),
puedes escribirnos vía e-mail a [am-orange b]#careers@autonomicmind.com#
indicándonos qué sucedió y nosotros te indicaremos cómo proceder.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_9"]
¿Qué pongo en el portafolio?
[role="faq_p w-80 w-90-l ma-auto dn faq_9_content"]
Las instrucciones y pautas para la elaboración del portafolio
las puedes encontrar [button badge]#link:../portafolio/[aquí]#

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_10"]
No tengo +VISA+, ¿qué pongo en ese campo?
[role="faq_p w-80 w-90-l ma-auto dn faq_10_content"]
Este campo no es obligatorio,
no lo diligencies si no tienes +VISA+.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_11"]
¿En este proceso se realiza algún estudio de riesgos con mis datos?
[role="faq_p w-80 w-90-l ma-auto dn faq_11_content"]
Sí, en la etapa previa a la contratación,
se realiza una confirmación de los datos del candidato
en centrales de riesgo y otras referencias.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_12"]
¿Es obligatorio realizar todas las etapas del proceso?
[role="faq_p w-80 w-90-l ma-auto dn faq_12_content"]
A medida que avances en el proceso de selección,
y dependiendo para qué vacante estás postulado,
se te darán indicaciones al respecto.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_13"]
Después de entrar
[role="faq_p w-80 w-90-l ma-auto dn faq_13_content"]
Aplicarán las políticas de la organización
que solicita la vacante para la cual te presentaste.
