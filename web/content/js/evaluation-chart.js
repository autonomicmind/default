/* eslint id-blacklist : "off"*/

const setTooltipsLabels = (tooltipItem, dat) => ` ${ dat.datasets[0]
  .data[tooltipItem.index] }%`;

const labels = [
  'Security',
  'Penetration Testing',
  'Networking',
  'Programming',
  'Java',
  'Python',
  'C#',
  'Audit',
  'SQL',
  'Cryptography',
  'Unix and GNU/Linux',
  'Software Engineering',
  'UML',
  'Project Management',
  'ISO 27001',
  'ISO 27002',
  'Automation',
  'Performance Testing',
  'Functional Testing',
  'Continuous Integration',
];

const perLabelOne = 70;
const perLabelTwo = 70;
const perLabelThree = 30;
const perLabelFour = 60;
const perLabelFive = 50;
const perLabelSix = 60;
const perLabelSeven = 60;
const perLabelEight = 50;
const perLabelNine = 80;
const perLabelTen = 70;
const perLabelEleven = 50;
const perLabelTwelve = 70;
const perLabelThirteen = 40;
const perLabelFourteen = 50;
const perLabelFifteen = 30;
const perLabelSixteen = 50;
const perLabelSeventeen = 90;
const perLabelEighteen = 80;
const perLabelNineteen = 70;
const perLabelTwenty = 80;

const data = {
  labels,
  datasets: [
    {
      fill: true,
      backgroundColor: 'rgba(248,173,37,0.2)',
      borderColor: 'rgba(248,173,37,1)',
      lineTension: 0.15,
      pointBorderColor: '#fff',
      pointBackgroundColor: 'rgba(248,173,37,1)',
      data: [
        perLabelOne,
        perLabelTwo,
        perLabelThree,
        perLabelFour,
        perLabelFive,
        perLabelSix,
        perLabelSeven,
        perLabelEight,
        perLabelNine,
        perLabelTen,
        perLabelEleven,
        perLabelTwelve,
        perLabelThirteen,
        perLabelFourteen,
        perLabelFifteen,
        perLabelSixteen,
        perLabelSeventeen,
        perLabelEighteen,
        perLabelNineteen,
        perLabelTwenty,
      ],
    },
  ],
};

const renderChart = (chartId, lblFontSize) => new Chart(chartId, {
  type: 'radar',
  data,
  options: {
    legend: {
      display: false,
    },
    scale: {
      angleLines: {
        display: true,
      },
      ticks: {
        suggestedMin: 20,
        suggestedMax: 100,
        stepSize: 20,
      },
      pointLabels: {
        fontSize: lblFontSize,
      },
    },
    title: {
      display: true,
      text: 'Distribution in %',
    },
    tooltips: {
      enabled: true,
      mode: 'label',
      callbacks: {
        label: setTooltipsLabels,
      },
    },
  },
});

const radarChartDoc = $(document).ready(() => {
  const chartId = $('#evaluation-chart');

  const checkWidth = () => {
    const width = window.innerWidth;
    const respWidth = 700;

    return width > respWidth;
  };

  const resizeChart = () => {
    const isDesktopVersion = checkWidth();

    if (isDesktopVersion) {
      const setHeight = chartId.height('20rem');
      const setWidth = chartId.width('20rem');
    }
    else {
      const setHeight = chartId.height('45vw');
      const setWidth = chartId.width('45vw');
    }

    return isDesktopVersion;
  };

  const waypointHandler = (dir) => {
    const TIME = 300;
    const wait = dir === 'down' && setTimeout(() => {
      const hideLoader = $('.loader').addClass('dn');
      const checkResponsive = checkWidth();
      const fontSizeDesktop = 12;
      const fontSizeMobile = 7;
      const labelsfontSize = checkResponsive ? fontSizeDesktop : fontSizeMobile;

      return renderChart(chartId, labelsfontSize);
    }, TIME);

    return $('.loader').waypoint('destroy');
  };

  const setChartSize = resizeChart();
  const resizeEvent = $(window).on('resize', resizeChart);

  return $('.loader').waypoint({
    handler: waypointHandler,
    offset: '100%',
  });
});
