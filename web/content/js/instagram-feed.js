const refreshLongLivedToken = (token) => $.ajax(`https://graph.instagram.com/
  refresh_access_token?grant_type=ig_refresh_token&access_token=${ token }`,
{
  dataType: 'json',
  timeout: 10000,
});

const getToken = () => `IGQVJVVlBCZAjE0NGROV25MdG9GUjMtZAUgwSlNmb0h1aXV6QTJDN
jk4UzJndzg3aUF4ZAGszZA29fMjBlalFiZADNVYjhpejVGak1GSmR0Uk5FR0FNR1lBLTdtNEQ3UV9
xVTRVOTh1MUVsOFR5M2R6ZAnhEQwZDZD`;

const successRequest = (dataSet) => {
  const NUM = 5;
  const feed = dataSet.data.map((value) => `<a class="w-50 w-20-l mh2 grow
  no-underline dib" target="_blank" href="${ value.permalink }">
  <img class="w-100" src="${ value.media_url }" alt="instagram post">
  </a>`
  );
  const containerId = $('#inner-container').append(feed.slice(0, NUM).join(''));
  const hideLoader = $('#instagram-loader').addClass('dn');
  const instContainer = $('#instagram-container').removeClass('dn');

  return $.when(getToken()).then(refreshLongLivedToken);
};

const feedRequest = (accessToken) => $.ajax(`https://graph.instagram.com/me/
media?fields=id,media_url,permalink&access_token=${ accessToken }`,
{
  dataType: 'json',
  timeout: 5000,
  success: successRequest,
});

const instagramDoc = $(document).ready(() => {
  const NUM = 5;
  const TIME = 500;
  const HIDDEN = 3;

  const chevronRightBtn = $('#chevron-next-inst').click(() => {
    const WIDTH = $('#inner-container').width();
    const position = parseInt($('#inner-container').css('marginLeft'), 10);

    return Math.abs(position) < HIDDEN * (WIDTH / NUM) && $('#inner-container')
      .animate({ marginLeft: `${ position - (WIDTH / NUM) }px` }, TIME);
  });

  const chevronLeftBtn = $('#chevron-back-inst').click(() => {
    const WIDTH = $('#inner-container').width();
    const position = parseInt($('#inner-container').css('marginLeft'), 10);

    return position < 0 && $('#inner-container')
      .animate({ marginLeft: `${ position + (WIDTH / NUM) }px` }, TIME);
  });

  return $.when(getToken()).then(feedRequest);
});
