// here all related to the footer
const footerDoc = $(document).ready(() => {
  const TIME = 1250;

  return $('#top').click(() => $('html, body').animate({ scrollTop: 0 }, TIME));
});
