const barChartDoc = $(document).ready(() => {
  const options = {
    strokeWidth: 2,
    easing: 'easeOutCirc',
    duration: 1500,
    color: '#FF6600',
    trailColor: '#eee',
    trailWidth: 2,
    svgStyle: { width: '100%', height: '100%' },
    text: {
      autoStyleContainer: false,
      style: {
        color: '#FF6600',
        position: 'absolute',
        right: '0',
        top: '0',
      },
    },
    from: { color: '#FF8800' },
    to: { color: '#FF6600' },
    step: (state, evn) => {
      // greater value of the chart
      const MULTIPLIER = 14000;
      const setColor = evn.path.setAttribute('stroke', state.color);

      return evn.setText(Math.round(evn.value() * MULTIPLIER));
    },
  };

  const barOne = new ProgressBar.Line('#barOne', Object.assign({}, options,
    {
      text: {
        autoStyleContainer: false,
        style: {
          color: '#FF6600',
          position: 'absolute',
          right: '12%',
          top: '0',
        },
      },
    }));
  const barTwo = new ProgressBar.Line('#barTwo', Object.assign({}, options,
    {
      text: {
        autoStyleContainer: false,
        style: {
          color: '#FF6600',
          position: 'absolute',
          right: '65%',
          top: '0',
        },
      },
    }));

  return $('#loader-bar-chart').waypoint({
    handler: (dir) => {
      const TIME = 500;
      // percentages of bar chart
      const APPLY = 1;
      const TESTS = 0.322428571;

      return dir === 'down' && setTimeout(() => {
        const hideLoader = $('#loader-bar-chart').addClass('dn');
        const displayChart = $('.barChart').removeClass('dn');
        const animateBarone = barOne.animate(APPLY);

        return barTwo.animate(TESTS);
      }, TIME);
    },
    offset: '100%',
  });
});
