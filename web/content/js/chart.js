/* eslint id-blacklist : "off"*/

const perLabelOne = 32.0;
const perLabelTwo = 18.3;
const perLabelThree = 17.5;
const perLabelFour = 6.2;
const perLabelFive = 5.2;
const perLabelSix = 4.7;
const perLabelSeven = 3.8;
const perLabelEight = 2.3;
const perLabelNine = 2.0;
const perLabelTen = 1.9;
const perLabelEleven = 1.2;
const perLabelTwelve = 1.2;
const perLabelThirteen = 0.8;
const perLabelFourteen = 0.7;
const perLabelFifteen = 0.7;
const perLabelSixteen = 0.5;
const perLabelSeventeen = 0.5;
const perLabelEighteen = 0.2;

const items = {
  datasets: [
    {
      data: [
        perLabelOne,
        perLabelTwo,
        perLabelThree,
        perLabelFour,
        perLabelFive,
        perLabelSix,
        perLabelSeven,
        perLabelEight,
        perLabelNine,
        perLabelTen,
        perLabelEleven,
        perLabelTwelve,
        perLabelThirteen,
        perLabelFourteen,
        perLabelFifteen,
        perLabelSixteen,
        perLabelSeventeen,
        perLabelEighteen,
      ],
      backgroundColor: [
        'rgba(40, 139, 226, 1)',
        'rgba(0, 204, 177, 1)',
        'rgba(255, 102, 0, 1)',
        'rgba(248, 135, 42, 1)',
        'rgba(145, 208, 144, 1)',
        'rgba(143, 113, 204, 1)',
        'rgba(29, 81, 144, 1)',
        'rgba(148, 217, 38, 1)',
        'rgba(86, 110, 205, 1)',
        'rgba(170, 120, 60, 1)',
        'rgba(242, 195, 125, 1)',
        'rgba(175, 233, 220, 1)',
        'rgba(89, 206, 232, 1)',
        'rgba(140, 102, 8, 1)',
        'rgba(0, 107, 96, 1)',
        'rgba(127, 61, 70, 1)',
        'rgba(193, 66, 51, 1)',
        'rgba(182, 176, 104, 1)',
      ],
    },
  ],
  labels: [
    'Software Development',
    'Project Management',
    'Other',
    'Information Security',
    'Data Scientist',
    'Test Automation',
    'Ethical Hacker',
    'DevOps',
    'Marketing',
    'Functional Testing & QA',
    'Technical Leader',
    'Software Architect',
    'Sales',
    'UX / UI Designer',
    'Help Desk',
    'Machine Learning',
    'Human Talent',
    'Cloud',
  ],
};

const setLegends = (chart) => {
  const [ element ] = chart.data.datasets;
  const legends = element.data.map((value, ind) => `<li>
  <span class="chart-legend"
    style="background-color:${ element.backgroundColor[ind] }">
  </span>
  <span class="chart-legend-label-text">
    ${ value }% ${ chart.data.labels[ind] }
  </span>
  </li>`);

  return `<ul>${ legends.join('') }</ul>`;
};

const setTooltipsLabels = (tooltipItem, data) => ` ${ data.datasets[0]
  .data[tooltipItem.index] }% ${ data.labels[tooltipItem.index] }`;

const chartDoc = $(document).ready(() => {
  const chartId = $('#rolesChart');
  const innerText = '13,760';

  const renderChart = () => {
    const chartRegister = Chart.pluginService.register({
      beforeDraw: (chart) => {
        const { width } = chart.chart;
        const { height } = chart.chart;
        const { ctx } = chart.chart;
        const ctxRestore = ctx.restore();
        const FONTDIVISOR = 200;
        const fontSize = (width / FONTDIVISOR).toFixed(2);
        // disabled due to the way Chart.js allow styles customization
        /* eslint-disable fp/no-mutation */
        ctx.font = `${ fontSize }em fantasy`;
        ctx.fillStyle = '#404040';
        ctx.textBaseline = 'middle';
        /* eslint-enable fp/no-mutation */
        const { text } = chart.config.options.elements.center;
        const textX = Math.round((width - ctx.measureText(text).width) / 2);
        const TEXTYDIVISOR = 2.2;
        const textY = height / TEXTYDIVISOR;
        const fillText = ctx.fillText(text, textX, textY);

        return ctx.save();
      },
    });

    const myChart = new Chart(chartId, {
      type: 'doughnut',
      animation: {
        animateScale: true,
      },
      data: items,
      options: {
        maintainAspectRatio: false,
        elements: {
          center: {
            text: innerText,
          },
        },
        responsive: true,
        legend: false,
        legendCallback: setLegends,
        tooltips: {
          enabled: true,
          mode: 'label',
          callbacks: {
            label: setTooltipsLabels,
          },
        },
      },
    });

    return $('#my-legend-con').html(myChart.generateLegend());
  };

  const responsive = () => {
    const width = window.innerWidth;
    const respWidth = 700;

    if (width > respWidth) {
      const setHeight = chartId.height('26rem');
      const setWidth = chartId.width('26rem');
    }
    else {
      const setHeight = chartId.height('45vw');
      const setWidth = chartId.width('45vw');
    }

    return width;
  };

  const waypointHandler = (dir) => {
    const TIME = 300;
    const wait = dir === 'down' && setTimeout(() => {
      const hideLoader = $('.loader').addClass('dn');
      const displayChart = $('.roles-chart').removeClass('dn');

      return renderChart();
    }, TIME);

    return $('.loader').waypoint('destroy');
  };

  const checkResponsive = responsive();
  const resizeEvent = $(window).on('resize', responsive);

  return $('.loader').waypoint({
    handler: waypointHandler,
    offset: '100%',
  });
});
