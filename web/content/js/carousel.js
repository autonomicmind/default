// Script to animate the Clients' image carousel
const carouselDoc = $(document).ready(() => {
  const TRANSITION = 1000;
  const PERIOD = 4000;

  const slider = $('#slider');
  const firstSection = '#slider section:first';
  const lastSection = '#slider section:last';

  const getParameters = () => {
    const STARTMOBILE = 75;
    const STEPMOBILE = 40;
    const STARTDESKTOP = 21;
    const STEPDESKTOP = 15;
    const WIDTHLIMIT = 960;
    const width = window.innerWidth;
    const [ start, step ] = (width >= WIDTHLIMIT) ?
      [ STARTDESKTOP, STEPDESKTOP ] :
      [ STARTMOBILE, STEPMOBILE ];

    return [ start, start + step, start - step ];
  };

  const initialize = () => {
    const [ start ] = getParameters();
    const queue = $('#slider section:last').insertBefore(firstSection);

    return slider.css('margin-left', `-${ start }%`);
  };

  const forward = () => {
    const [ start, advance, _ ] = getParameters();

    return slider.animate({ marginLeft: `-${ advance }%` }, TRANSITION, () => {
      const forwardQueue = $(firstSection).insertAfter(lastSection);

      return slider.css('margin-left', `-${ start }%`);
    });
  };

  const backward = () => {
    const [ start, _, setback ] = getParameters();

    return slider.animate({ marginLeft: `-${ setback }%` }, TRANSITION, () => {
      const backwardQueue = $(lastSection).insertBefore(firstSection);

      return slider.css('margin-left', `-${ start }%`);
    });
  };

  const rightButton = $('#next-btn').on('click', () => forward());
  const leftButton = $('#back-btn').on('click', () => backward());
  const startShift = initialize();

  return setInterval(forward, PERIOD);
});
