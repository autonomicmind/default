// Script to create dropdown menus
const menuDoc = $(document).ready(() => {
  // Dark Dropdown menu
  const ddmt = '.ddm-dark-title';
  const ddmp = '.ddm-dark-p';
  const titlepClasses = 'pl4 mt0 f5 f4-l white tracked mv3 left-0';
  const iconHtml = `<span class="flex items-center mr4">
                      <i class="white fas fa-chevron-down"></i>
                    </span>`;
  const titleClasses = $(ddmt).addClass('flex justify-between bg-mid-gray');
  const titleparagraph = $(`${ ddmt } p`).addClass(`${ titlepClasses }`);
  const icon = $(iconHtml).insertAfter(`${ ddmt } p`);
  const paragraph = $(ddmp).addClass('ph3 ph4-l pv3 bt br bb bl b--black-20');
  const pClasses = $(`${ ddmp } p`).addClass('mt0 mb3 f5 f4-l am-gray tj');
  const listClasses = $(`${ ddmp } li`).addClass('am-gray f5 f4-l');
  const listParagraphClasses = $(`${ ddmp } li p`).addClass('pl1');
  const lists = $(`${ ddmp } ol, ${ ddmp } ul`).addClass('pl3 pl4-l');
  const bidentClasses = $('.bident').addClass('pl0');

  return $(`${ ddmt }`).on('click', (evn) => {
    const id = evn.target.id ? evn.target.id : evn.target.parentNode.id;
    const contentId = `.${ id }-content`;

    return $(contentId).slideToggle();
  });
});
