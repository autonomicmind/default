const MOBILEROWS = 3;
const DESKTOPROWS = 2;

const getDisplayedRows = () => {
  const width = window.innerWidth;
  const respWidth = 960;

  return width > respWidth ? DESKTOPROWS : MOBILEROWS;
};

const paginationDoc = $(document).ready(() => {
  const COLUMNS = 3;
  const articleInnerContainer = $('#article-inner-container');
  const artContainer = $('#article-container');
  const pagCan = $('#pagination-container');
  const numArticles = $('#article-container #article-inner-container').length;
  const numRows = Math.ceil(numArticles / COLUMNS);
  const addSelectedClass = $('.btn-page-container:first').addClass('selected');
  const initialBtn = $('.btn-page:first').addClass('white bg-mid-gray');

  const setSize = () => {
    const displayedRows = getDisplayedRows();
    const responsiveBtns = displayedRows === 2 ?
      $('.btn-page-mobile').addClass('dn') :
      $('.btn-page-mobile').removeClass('dn');
    const innerHeight = articleInnerContainer.height();
    const pag = pagCan.css('height', `${ displayedRows * innerHeight }px`);
    const height = artContainer.css('height', `${ numRows * innerHeight }px`);

    return artContainer.css('margin-top', '0px');
  };

  const waypointEvent = pagCan.waypoint({
    handler: setSize,
    offset: '100%',
  });

  const selection = (btnSelectedId, btnSelected) => {
    const innerHeight = articleInnerContainer.height();
    const displayedRows = getDisplayedRows();
    const btns = displayedRows * (parseInt(btnSelectedId, 10) - 1);
    const art = artContainer.css('margin-top', `-${ btns * innerHeight }px`);
    const removeSelected = $('div.selected').removeClass('selected');
    const addSelected = btnSelected.addClass('selected');
    const other = $('.btn-page-container a').removeClass('white bg-mid-gray');

    return $('.selected a').addClass('white bg-mid-gray');
  };

  const buttons = $('.btn-page-container').click((evn) => {
    const id = evn.target.id ? evn.target.id : evn.target.parentNode.id;
    const innerHeight = articleInnerContainer.height();
    const displayedRows = getDisplayedRows();
    const btnPageId = displayedRows * (parseInt(id, 10) - 1);
    const removeSelected = $('.btn-page-container').removeClass('selected');
    const addSelected = $(`#${ id }`).addClass('selected');
    const others = $('.btn-page').removeClass('white bg-mid-gray');
    const currentBtn = $('.selected a').addClass('white bg-mid-gray');

    return artContainer.css('margin-top', `-${ btnPageId * innerHeight }px`);
  });

  const rightChevron = $('#articles-right').click((evn) => {
    const id = evn.target.id ?
      evn.target.id :
      evn.target.parentNode.parentNode.id;
    const btnSelected = $('div.selected').next();
    const btnSelectedId = btnSelected.attr('id');
    const isDisplayNone = btnSelected.hasClass('dn');

    return (btnSelectedId !== id && !isDisplayNone) && selection(btnSelectedId,
      btnSelected);
  });

  const leftChevron = $('#articles-left').click((evn) => {
    const id = evn.target.id ?
      evn.target.id :
      evn.target.parentNode.parentNode.id;
    const btnSelected = $('div.selected').prev();
    const btnSelectedId = btnSelected.attr('id');
    const isDisplayNone = btnSelected.hasClass('dn');

    return (btnSelectedId !== id && !isDisplayNone) && selection(btnSelectedId,
      btnSelected);
  });

  return $(window).on('resize', setSize);
});
