// articles elements classes
const articlesDoc = $(document).ready(() => {
  const containerClasses = $(`.content h2, .content h3, .content .paragraph,
  .content > .olist, .content > .ulist, .sectionbody > .olist,
  .sectionbody > .ulist, .sect2 > .olist, .sect2 > .ulist`)
    .addClass('w-80 ml-auto mr-auto mw9');
  const secondaryHeaderClasses = $('.content h2').addClass('f3 f2-l\n' +
    'am-orange tracked normal tc mv0');
  const tertiaryHeaderClasses = $('.content h3').addClass('f4 f3-l am-gray\n' +
    'semi-tracked tl');
  const ParagraphClasses = $('.content p, .content ol').addClass('f5 f4-l\n' +
    'am-gray semi-tracked tj');

  return $('.content li p').addClass('mv0');
});
