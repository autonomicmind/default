const desktopBannerNewLines = () => {
  const title = $('#section-title').html().replace(/\\ln/g, '<br>')
    .replace(/\\n/g, '');
  const subtitle = $('#section-subtitle').html().replace(/\\ln/g, '<br>')
    .replace(/\\n/g, '');
  const newTitle = $('#section-title').html(title);

  return $('#section-subtitle').html(subtitle);
};

const mobileBannerNewLines = () => {
  const title = $('#section-title').html().replace(/\\ln/g, '')
    .replace(/\\n/g, '<br>');
  const subtitle = $('#section-subtitle').html().replace(/\\ln/g, '')
    .replace(/\\n/g, '<br>');
  const newTitle = $('#section-title').html(title);

  return $('#section-subtitle').html(subtitle);
};

const removeNewLinesLarge = () => $('.del-br-l').map((ind, evn) => {
  const sel = `${ evn.localName }.${ evn.className }`.replace(/ /g, '.');

  return $(sel).html(evn.innerHTML.replace(/<br>/g, ''));
});

const responsive = () => {
  const width = window.innerWidth;
  const respWidth = 960;
  const menuliId = $('.menuli');
  const processImg = $('.process');
  const isSpanishImg = processImg.hasClass('es');

  if (width > respWidth) {
    const setMargin = $('#inner-container').css('margin-left', 'auto');
    const tel = $('a[href^="tel:"]').click((event) => event.preventDefault());
    const addGrow = menuliId.addClass('grow');
    const linesLarge = removeNewLinesLarge();
    const newLines = desktopBannerNewLines();
    const setImg = isSpanishImg ?
      processImg.attr('src', '/images/infographics/proceso-h.png') :
      processImg.attr('src', '/images/infographics/process-h.png');
  }
  else {
    const removeGrow = menuliId.removeClass('grow');
    const mobileNewLines = mobileBannerNewLines();
    const setImg = isSpanishImg ?
      processImg.attr('src', '/images/infographics/proceso-v.png') :
      processImg.attr('src', '/images/infographics/process-v.png');
  }

  return isSpanishImg;
};

const responsiveDoc = $(document).ready(() => {
  const res = responsive();

  return $(window).on('resize', responsive);
});
