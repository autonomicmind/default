const addResponsiveClasses = () => $('.img_left_br_a img')
  .addClass('br6 br--right');

const removeResponsiveClasses = () => $('.img_left_br_a img')
  .removeClass('br--right');

const responsibleTable = () => {
  const table = $('.tableblock td:nth-child(even)').map((key, evn) => {
    if (evn.parentNode) {
      const removeElement = evn.parentNode.removeChild(evn);
    }
    const tableId = `#table_${ key }`;

    return $(`<tr><td>${ evn.innerHTML }</td></tr>`)
      .insertAfter(`${ tableId } tr`);
  });

  return $('.tableblock td').attr('colspan', 2);
};

const desktopTable = () => {
  const table = $('.tableblock tr:nth-child(even)').map((key, evn) => {
    if (evn.parentNode) {
      const removeElement = evn.parentNode.removeChild(evn);
    }
    const tableId = `#table_${ key }`;

    return $(evn.innerHTML).insertAfter(`${ tableId } td`);
  });

  return $('.tableblock td').attr('colspan', 1);
};

const resizeTable = () => {
  const rowCount = $('#table_0 tr').length;
  const WIDTHLIMIT = 960;
  const width = window.innerWidth;

  if (width > WIDTHLIMIT) {
    if (rowCount > 1) {
      const resultRemoveResponsiveClasses = removeResponsiveClasses();
      const resultDesktopTable = desktopTable();
    }
  }
  else if (rowCount < 2) {
    const resultAddResponsiveClasses = addResponsiveClasses();
    const resultResponsibleTable = responsibleTable();
  }

  return width > WIDTHLIMIT;
};

const docContent = $(document).ready(() => {
  const resultResizeTable = resizeTable();
  const sectionTitleClasses = $('.section_title p').addClass('f3 f2-l\n' +
    'normal am-gray tracked mv2 mv3-l mh3 pl3 pl5-l');
  const sectionLeftParagraphClasses = $('.section_p_l p').addClass('f5 f4-l\n' +
    'normal am-gray pt0 mt0 mh3 pl4 pl5-l pr3 pr5-l');
  const sectionRightParagraphClasses = $('.section_p_r p').addClass('f5\n' +
    'f4-l normal am-gray pt0 mt0 mh3 pl3 pl5-l pr3 pr6-l');
  const sectionListClasses = $('.section_ul').addClass('pl2 pl5-l pr4 pr6-l\n' +
    'pb3 pb0-l');
  const sectionListParagraphClasses = $('.section_ul p').addClass('f5 f4-l\n' +
    ' normal am-gray pt0 mt0 pb0 mb0 pl2');
  const tableClasses = $('.table').addClass('mw9 ma-auto mb3 mb6-l');
  const imgRightClasses = $('.img_right img').addClass('br6 br--left w-90\n' +
    ' w-100-l');
  const imgRightContent = $('.img_right .content').addClass('tr mb4 mb5-l\n' +
    'mb0-l');
  const imgLeftClasses = $('.img_left_br_a img').addClass('br6 ml5-l w-90');
  const imgLeftContentClasses = $('.img_left_br_a .content').addClass('tl');
  const firstTableClasses = $('.table:first').addClass('mt5 mt0-l');
  const lastTableClasses = $('.table:last').addClass('mb5 mt0-l');
  const sliderTitleClasses = $('.title_slider h2').addClass('f3 f2-l normal\n' +
    'tracked ph5 ph0-l');

  // FAQ section
  const faqTitleClasses = $('.faq_title').addClass('flex');
  const faqTitleParagraphClasses = $('.faq_title p').addClass('pl3 mt0 f5\n' +
    'f3-l am-gray tracked');
  const faqParagraphClasses = $('.faq_p p').addClass('mt0 mb4 f5 f4-l\n' +
    'am-gray tj');
  const badge = $('.badge a').addClass('br4 pb1 b ph2 white am-bg-orange');
  const faqIcon = $('<span class="am-orange f4">&#10095;</span>')
    .insertBefore('.faq_title p');
  const faqSeparator = $('<div class="w-80 w-90-l ba b--black-10 ma-auto\n' +
    'mb4"></div>').insertBefore('.faq_title');
  const faqLastSeparator = $('<div class="w-80 w-90-l ba b--black-10\n' +
    'ma-auto mb4"></div>').insertAfter('.faq_p:last');

  // content
  const listClasses = $('.ulist ul').addClass('list');
  const listParagraphClasses = $('.ulist li p').addClass('dib');
  const itemClasses = $('.item').addClass('w-80 ma-auto mw9');
  const itemSecondaryHeaderClasses = $('.item h2').addClass('f3 f2-l\n' +
    'am-orange tracked normal tc mv0');
  const itemTertiaryHeaderClasses = $('.item h3').addClass('f4 f3-l am-gray\n' +
    'semi-tracked tl');
  const itemParagraphClasses = $('.item p, .item ol').addClass('f5 f4-l\n' +
    'am-gray semi-tracked tj');
  const lastHeaderClasses = $('.end h2').addClass('f4 f3-l am-gray\n' +
    'semi-tracked normal tc mv0 ph5');
  const itemListParagraphClasses = $('.item li p').addClass('mv2');
  const listStyle = $('li:has(.lsn)').css('list-style', 'none');

  // FAQ click event
  const faqClick = $('.faq_title').on('click', (evn) => {
    const id = evn.target.id ? evn.target.id : evn.target.parentNode.id;
    const contentId = `.${ id }_content`;
    const contentHidden = $(contentId).is(':hidden');

    if (contentHidden) {
      const removeGrayColor = $(`#${ id } p`).removeClass('am-gray');
      const addOrangeColor = $(`#${ id } p`).addClass('am-orange');
    }
    else {
      const removeOrangeColor = $(`#${ id } p`).removeClass('am-orange');
      const addGrayColor = $(`#${ id } p`).addClass('am-gray');
    }

    return $(contentId).slideToggle();
  });

  // dropdown menu of candidates section
  const firstOption = $('#btn-0').click(() => $('#box-0').slideToggle());
  const secondOption = $('#btn-1').click(() => $('#box-1').slideToggle());
  const thirdOption = $('#btn-2').click(() => $('#box-2').slideToggle());
  const fourthOption = $('#btn-3').click(() => $('#box-3').slideToggle());
  const fifthOption = $('#btn-4').click(() => $('#box-4').slideToggle());
  const sixthOption = $('#btn-5').click(() => $('#box-5').slideToggle());
  const seventhOption = $('#btn-6').click(() => $('#box-6').slideToggle());

  return $(window).on('resize', resizeTable);
});
