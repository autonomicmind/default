const isFullNameValid = (fullName) => {
  const pattern = /^[A-Za-zÁ-úñÑ\s]{2,25}$/g;

  return pattern.test(fullName);
};

const isEmailValid = (email) => {
  const pattern = /^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/ig;

  return pattern.test(email);
};

const isCompanyValid = (company) => {
  const pattern = /^[A-Za-zÁ-úñÑ\s]{2,25}$/g;

  return pattern.test(company);
};

const addWrongClass = (validation, formIds, fieldId) => {
  const validate = validation.map((element, index) => {
    if (formIds[index] === `#${ fieldId }`) {
      const isElementWrong = element ?
        $(formIds[index]).removeClass('wrong-input') :
        $(formIds[index]).addClass('wrong-input');
    }

    return element;
  });

  return validate[0] && validate[1] && validate[2];
};

const validateData = (dataSet, fieldId) => {
  const submit = $(':submit');
  const isCompanyRight = isCompanyValid(dataSet.Company);
  const isEmailRight = isEmailValid(dataSet.Email);
  const isFullNameRight = isFullNameValid(dataSet['Full Name']);
  const validation = [ isFullNameRight, isEmailRight, isCompanyRight ];
  const formIds = [ '#mce-FULL-NAME', '#mce-EMAIL', '#mce-COMPANY' ];
  const isValidationRight = addWrongClass(validation, formIds, fieldId);

  return isValidationRight ?
    submit.attr('disabled', false) :
    submit.attr('disabled', true);
};

const formDoc = $(document).ready(() => {
  const inputs = $('.contact-form');

  return inputs.on('input', (event) => {
    const fieldId = event.target.id;
    const namesValue = $('#mce-FULL-NAME').val();
    const emailValue = $('#mce-EMAIL').val();
    const companyValue = $('#mce-COMPANY').val();
    const formData = {
      'Full Name': namesValue,
      'Email': emailValue,
      'Mobile': '',
      'Company': companyValue,
      'Description': '',
    };

    return validateData(formData, fieldId);
  });
});
