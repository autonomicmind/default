const toggleMenu = () => {
  const navMenu = $('.am-navbar');
  const isNavBarOpen = navMenu.hasClass('am-navbar-open');

  if (isNavBarOpen) {
    const navBarClose = navMenu.removeClass('am-navbar-open');
    const displayBtn = $('.am-button span:first').removeClass('dn');
    const hidenBtn = $('.am-button span:last').addClass('dn');
  }
  else {
    const navBarOpen = navMenu.addClass('am-navbar-open');
    const hideBtn = $('.am-button span:first').addClass('dn');
    const displayBtn = $('.am-button span:last').removeClass('dn');
  }

  return isNavBarOpen;
};

const scrollMenu = () => {
  const firstSpan = $('.am-button span i:first');
  const header = $('.am-header');
  const scroll = $(window).scrollTop();
  const scrolledClass = header.hasClass('header-scrolled');
  const colorBanner = header.hasClass('white-banner');
  const logoId = $('#logoImg');
  const isHomeSection = $('.am-button').hasClass('home');

  if (scroll !== 0 && !scrolledClass) {
    const scrollHeader = header.addClass('header-scrolled');
    const setLogo = logoId.attr('src', '/images/logo.png');
    const removeWhiteColor = firstSpan.removeClass('white');
    const setGrayColor = firstSpan.addClass('am-light-gray');
  }
  else if (scroll === 0 && scrolledClass) {
    const removeScroll = header.removeClass('header-scrolled');
    const logo = colorBanner && logoId.attr('src', '/images/logo-white.png');

    if (isHomeSection) {
      const removegray = firstSpan.removeClass('am-light-gray');
      const addWhiteColor = firstSpan.addClass('white');
    }
  }

  return scrolledClass;
};

const headerDoc = $(document).ready(() => {
  const ddm = $('#dropdown-menu');
  const ddmClick = ddm.click(() => $('#dropdown-content').slideToggle());
  const menuBtnClick = $('.am-button').click(toggleMenu);

  return window.addEventListener('scroll', scrollMenu);
});
