---
title: FAQ
subtitle: 
description: >
  Page meant for recruitment process applicants to find answers to their most
  common questions
keywords:
  - 'faqs'
  - 'questions'
  - 'answers'
  - 'doubts'
  - 'thoughts'
banner_title: Frequently asked questions\n (FAQ)
lang: en
---

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_1"]
Why, if I already applied through a job portal, 
I have not been invited to the process yet?
[role="faq_p w-80 w-90-l ma-auto dn faq_1_content"]
Once you apply to one of our vacancies
in one of the portals on which we are registered,
we can take from 1 to 2 days in identifying potential candidates,
classifying and inviting via e-mail.
If you have not been invited
after 2 days (excluding weekends)
and you think your profile fits the vacancy,
you can write to us to careers@autonomicmind.com.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_2"]
The exam application won't let me get out from Moodle, what can I do?
[role="faq_p w-80 w-90-l ma-auto dn faq_2_content"]
If you have already finished the exam,
click on the button located
on the lower-right side of the screen to exit.
If it does not work, you can try restarting the computer,
as by that time the exam would be successfully sent
and no data will be lost.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_3"]
I do not know for which vacancy I am applying, how can I find out?
[role="faq_p w-80 w-90-l ma-auto dn faq_3_content"]
You can let us know any doubt or observation
regarding the selection process to the email careers@autonomicmind.com.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_4"]
I already applied for one vacancy,
can I do it again for another one or for the same one
[role="faq_p w-80 w-90-l ma-auto dn faq_4_content"]
Yes, you can.
In case 6 months have passed after your last application,
you must start the process from the beginning.
In case less than 6 months have passed after your last application,
email us at careers@autonomicmind.com
letting us know that you want to apply to another vacancy.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_5"]
Can I apply to two different vacancies?
[role="faq_p w-80 w-90-l ma-auto dn faq_5_content"]
Yes, you only need to do the process once
and let us know to which vacancies you want
to apply to careers@autonomicmind.com

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_6"]
How much experience is required to apply to the vacancies?
[role="faq_p w-80 w-90-l ma-auto dn faq_6_content"]
In the [button badge]#link:../vacancies/[vacancies]# page
you can find the required experience for each vacancy.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_7"]
The exam application is not working, what can I do??
[role="faq_p w-80 w-90-l ma-auto dn faq_7_content"]
Once you download and install the SafeExamBrowser application,
do not open it right away.
Instead,
Run the file “fluidexams.seb”,
located inside the file you downloaded.
If the problem persists, ty:

[role="am-gray f4 w-80 w-90-l ma-auto dn faq_7_content ws"]
** Open the file as administrator
** Try from a different PC
** In case you don't have Windows as OS,
you can download the application [button badge]#link:https://safeexambrowser.org/download_en.html[here]#

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_8"]
I could not present the test, what can I do?
[role="faq_p w-80 w-90-l ma-auto dn faq_8_content"]
In case you have a justifiable reason
(Application blocking, unexpected restarts, connection problems),
you can write to us to careers@autonomicmind.com
telling us what happened
and we will let you know how to proceed

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_9"]
What can I include in my portfolio?
[role="faq_p w-80 w-90-l ma-auto dn faq_9_content"]
Instructions and guidelines for creating your portfolio
can be found [button badge]#link:../portfolio/[here]#

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_10"]
I do not have a +VISA+, how should I fill that field?
[role="faq_p w-80 w-90-l ma-auto dn faq_10_content"]
This field is not mandatory,
you can leave it empty if you do not have a +VISA+

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_11"]
Is there any kind of risk study done with the data
you collected throughout the process?
[role="faq_p w-80 w-90-l ma-auto dn faq_11_content"]
Yes, during the stage right before hiring,
a confirmation of the applicant data in
risk centrals and other places is made.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_12"]
Is it mandatory to finish all the process stages?
[role="faq_p w-80 w-90-l ma-auto dn faq_12_content"]
As you go through the process,
depending on what vacancy you are applying for,
you will be given information regarding this.

[role="faq_title w-80 w-90-l ma-auto pointer" id="faq_13"]
After being hired
[role="faq_p w-80 w-90-l ma-auto dn faq_13_content"]
All the hiring organization's rules and policies
will start applying.
