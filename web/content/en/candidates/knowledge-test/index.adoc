---
section: Child
title: Knowledge test
subtitle: 
description: >
  This page documents everything that an applicant needs to know
  to successfully present the technical test
keywords:
  - 'test'
  - 'technical'
  - 'software'
  - 'conditions'
  - 'SafeExamBrowser'
banner_title: Knowledge test
lang: en
---

:form: https://autonomicgeneralknowledgetest.thinkexam.com/startTest/NjEwNDI=/NA

[.item]
The next step to continue with your selection process
is to take our knowledge test.
This test contains several areas of knowledge,
some that apply to your profile, and some that do not.
This stage aims to highlight the knowledge areas
in which you have the best skills and abilities.

++++
<div class="loader ma-auto mv6 mv7-l"></div>
<div id="chartjs-radar" class="w-90 w-40-l ma-auto mt4 mt5-l mb5 mw9">
  <canvas id="evaluation-chart"></canvas>
</div>
++++

[role="ddm-dark-title w-80 mw9 ma-auto pointer" id="ddm-dark-option-1"]
Create an account
[role="ddm-dark-p w-80 mw9 ma-auto dn ddm-dark-option-1-content"]
. Go to the following [badge]#{form}[web address, window="_blank"]#.

. Fill out the form with personal data.

. Select a group according to the vacancy you are applying for.
If you are applying to:

.. Accounting Assistant or similar vacancies,
select the "Finance General Knowledge Test" group.

.. Ethical Hacker, QA, Developer, DevOps, or related,
choose the "General Knowledge Test" group.

[role="ddm-dark-title mt3 w-80 mw9 ma-auto pointer" id="ddm-dark-option-3"]
Important!
[role="ddm-dark-p w-80 mw9 ma-auto dn ddm-dark-option-3-content"]
. *You only have one chance to take the exam.*
Make sure you have the time and willingness to take it.

. Enter your full name.

. The exam lasts a maximum of *3 hours and 20 minutes*,
so you should have enough time to take it, without interruptions.

. Avoid leaving the main screen of the exam;
if you do, it will be automatically canceled,
and there will not be a second attempt.

. If you have any questions regarding this exam,
please contact: careers@autonomicmind.com

. *Send us an email notifying us that you have completed the exam.*

[.item]
[mv4 tracked]#Have Fun!  ... and good luck, but you don't need it.#
