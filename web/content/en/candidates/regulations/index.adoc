---
section: Child
title: Employment Regulations
subtitle: AUTONOMIC S.A.S.
description: >
  The purpose of this regulation is to determine the conditions,
  characteristics and qualities of the provision of employment management
  and placement services, the rights and duties of users.
keywords:
  - 'Autonomic Mind'
  - 'Employment'
  - 'regulations'
banner_title: MANAGEMENT OF EMPLOYMENT\n\ln REGULATIONS
lang: en
---

[.item]
== OBJECTIVE
{nbsp} +

The purpose of this regulation is to determine the conditions, 
characteristics and qualities of the provision of employment management and 
placement services, the rights and duties of users. 
[f5 b]#AUTONOMIC S.A.S.# regulates the provision of the service as a private 
lucrative Agency for employment management and placement, 
in order to give guidance and clarity to its conditions, 
as well as the rights and duties of the users of the service.

=== 1. DESCRIPTION OF THE LEGAL PERSON

[f5 b]#AUTONOMIC S.A.S.#, is a legal entity, private law, 
located at Calle 67 # 52-20 Building Ruta N.

[f5 b]#NATURE. AUTONOMIC S.A.S.#, 
as an employment management and placement agency, 
is constituted as a private agency that is subject to 
the legal regime established for this type of agencies, 
in accordance with Decree 1072 of May 26, 2015. 
It is conceived as a lucrative private agency of management and job placement.

=== 2. BASIC EMPLOYMENT MANAGEMENT AND PLACEMENT SERVICES

Basic services are understood to be the management 
that [f5 b]#AUTONOMIC S.A.S.#  performs, 
in the need of an employer (plaintiff) seeking to fill a vacancy. 
Or when a natural person (bidder) freely and spontaneously, 
chooses to share his resume with 
[f5 b]#AUTONOMIC S.A.S.# In the first scenario, 
[f5 b]#AUTONOMIC S.A.S.# provides this service to an employer (claimant) 
under conditions of a rate established and previously accepted by 
the employer (claimant) and in the second scenario this service is 
provided free of charge to the search engine (offeror) in accordance with 
the provisions of article 2.2 .6.1.2.4 of decree 1072 of 2015.

In accordance with the provisions of article 2.2.6.1.2.17 of 
Decree 1072 of 2015, [f5 b]#AUTONOMIC S.A.S.#, 
as a private lucrative agency for the management and placement of employment, 
provides basic services, including a registry of bidders, 
plaintiffs and vacancies, preselection and referral, as described below:

[f5 b]#AUTONOMIC S.A.S.# is a company specialized in 
recruiting qualified personnel in the fields of information technology, 
developers in all types of languages, software quality testers, 
project leaders with high technical capabilities, among others.

[underline]#*The selection process*#

. Through this process, we seek to find and hire the top 10% of 
the population for each role and salary range required.
. With this process you want to select only the best to build a team:

- Elite.
- Durable.
- Disciplined.
- Responsible.
- Ethical.

. We have automated a large part of the selection process 
to optimize time so that, as a candidate, 
you can aspire to become part of our team in a more agile and simple way.
. The vast majority of the process is done online and 
depends on its availability and dedication, therefore, 
the duration of the process is 90% in your hands.
. Since the selection process is detailed and 
somewhat different from what other companies do, 
it is essential that you have carefully read 
the entire process that is detailed on our website. 
. If you continue and accept the terms of this regulation 
you are indicating that you understand the effort and 
dedication that this implies and you agree to be part of it.
{nbsp} +
You should also keep in mind that if during the process 
you are not explicitly notified of the end of it, 
it means that you are still in it and therefore, 
you are subject to analysis for any current or potential vacancy.

[underline]#*The candidate also recognizes that:*#

** By accepting these terms and conditions, 
you declare that your current or previous contract 
does not contain clauses (or these have expired) that 
disable to work in any function in an information security company.

** By accepting these terms and conditions, 
you understand that signing a contract between 
the parties will contain clauses of:

- Exclusivity.
- No competition.
- No solicitation.
- Confidentiality.
{nbsp} +
The previous clauses will have application during the labor relationship.

** By accepting these terms and conditions, you understand that 
there is a possibility that our offer is lower than 
your initial salary expectation.
This is because:
- For us, the equity of the salaries of the people in 
our current team and the consistency of this remuneration with 
their performance is a higher priority.
- Our perception of your potential performance 
may not match your initial salary expectation.

** By accepting these terms and conditions, 
you agree to be recommended for vacancies in our business partners.

** By default your records will remain in our database for future offers.
- If you want the data collected to be deleted, 
you must indicate it via email to info@autonomicmind.com

=== 3. CONDITIONS OF THE TECHNICAL SUPPORT AND*
*HOURS OF ATTENTION TO THE USERS.*

If you wish to apply to our vacancies write to careers@autonomicmind.com 
or call us at +57 300 496 3851
{nbsp} +
If you require recruitment support for your company, 
write to sales@autonomicmind.com or call us at +57 310 624 7670.

=== 4. INFORMATION PROCESSING.

[b f5]#AUTONOMIC S.A.S.#, as an employment management and 
placement agency will be authorized to use the data provided for 
the provision of the management and placement service, 
in accordance with the provisions of Law 1581 of 2012. 
The information referred to here will be available to 
Public Employment Service providers, authorized by the Ministry of Labor, 
for the purpose that was granted and with the restrictions established by 
Law 1581 of 2012, 
by incorporating the Information System of the Public Employment Service.

By virtue of the above, the holders of personal data will have the right to:

** Know, update and rectify the personal data obtained by 
[b f5]#AUTONOMIC S.A.S.#

** Be informed about the use that has been given to your personal data.

** Revoke the authorization and/or request the deletion of the data when in 
[b f5]#AUTONOMIC S.A.S.#, constitutional and legal principles, 
rights and guarantees are not respected.

** File complaints with the Superintendence of Industry and 
Commerce for violations of the provisions of these regulations.

=== 5. DUTIES AND RIGHTS OF EMPLOYEE OFFERS AND DEMANDERS.

*The duties of bidders are:*

** Provide truthful information and keep your resume updated.

** Inform when you are not interested in continuing 
to provide management and placement services.

** Authorize [b f5]#AUTONOMIC S.A.S.# to use and transfer 
your information for the purposes of the agreed services.

** Inform when you intend to revoke 
the authorization of personal data processing.

*Bidders' rights are:*

** Receive adequate and quality care from [b f5]#AUTONOMIC S.A.S#.

** Know the Regulation of Provision of Services of 
the agency management and employment placement.

** Be informed about the treatment that 
the personal data you provide will receive.

** Rectify the information recorded at any time.

** Receive free of charge the basic services of the Agency for Management and 
Employment Placement of [b f5]#AUTONOMIC S.A.S#.

** Receive prior information about the rates of 
the additional services you request.

** Be informed about the procedures established by 
[b f5]#AUTONOMIC S.A.S.# for the services you request.

** Submit complaints and claims to [b f5]#AUTONOMIC S.A.S.# related to 
your service as an Employment Management and Placement Agency and 
that these are attended to within fifteen (15) calendar days of 
receiving the communication.

** Receive truthful and timely information about the world of work.

** Compete on equal terms for any job opportunity that is consistent with 
your profile and experience.

** Not to be discriminated for any reason whatever their race, origin, 
beliefs, religion, among others.

** Be treated with respect, kindness and cordiality.

** Rectify information that you have submitted or to request that 
your information be updated according to the evolution of your experience, 
studies, development of skills, etc.

** Receive adequate and quality care.

*The duties of the plaintiffs are:*

** Provide truthful information related to available vacancies, 
informing modifications and adjustments to the requested profiles.

** Inform [b f5]#AUTONOMIC S.A.S.# in writing when the vacancy is already 
filled and does not require services.

** Cancel the commission, where applicable, for the services received.

** Authorize [b f5]#AUTONOMIC S.A.S.# to use your information for the 
purposes of the agreed services.

** At the time that a candidate is linked, send a copy of the employment 
contract that shows at least the date of connection, 
the salary and the signatures thereof. 
Other information can be deleted or censored.

*The rights of the plaintiffs are:*

** Receive an adequate and quality service from [b f5]#AUTONOMIC S.A.S#. 

** Know the Regulation of Provision of Services of 
the agency management and employment placement.

** Be informed about the treatment that the personal data that you provide to 
[b f5]#AUTONOMIC S.A.S.# will receive.

** Rectify the information registered through [b f5]#AUTONOMIC S.A.S.# 
anytime.

** Be informed about the procedures established by [b f5]#AUTONOMIC S.A.S.# 
for the services you request.

** Submit complaints and claims to [b f5]#AUTONOMIC S.A.S.# related to its 
service as an Employment Management and Placement Agency and that these are 
attended to within fifteen (15) calendar days of receiving the communication.

** Receive information about the cost of the other services you requested by 
[b f5]#AUTONOMIC S.A.S.# when available.

** Know the steps taken by the employment placement agency in 
development of the service provision.

=== 6. SERVICE GUARANTEE.

A guarantee of the service shall be understood as any claim within 120 days 
from the date of commencement of the contract or of linking the candidate 
under the same profile of the candidate initially recruited, 
who meets any of the following conditions:

** Resignation by the candidate without just cause in accordance with 
paragraph 2 of article 47 of the Substantive Labor Code.

** Dismissal of the candidate for just cause in accordance with numeral a of 
article 62 of the Substantive Labor Code and with due process established by 
jurisprudence.

It is not understood to be included in the guarantee:

** No project completed by the client.

** Termination of the contract by the candidate arguing a just cause in 
accordance with section b of article 62 of the Substantive Labor Code.

** Lack of budget to cover the payroll of the candidate or any other cause 
not expressly stipulated in the guarantee.

To access the guarantee, a document signed by the legal representative must 
be sent, raising the request and arguing the reason for the guarantee, 
as well as sending any other document that is required 
in order to validate the request. 
In case of not presenting the required documentation 
within the following 5 business days, 
the guarantee will be understood as withdrawn.


=== 7. RIGHTS AND OBLIGATIONS OF THE INTERMEDIARY AND THE OFFERER AGAINST THE (MANUFACTURER DEMANDER) LABOR CLAIMANT.

** The intermediary and the labor bidder have the right 
to have the labor plaintiff deliver truthful, accurate, 
and respectful information of the applicable laws in force.

** The intermediary and the labor bidder have the right to demand that 
the labor claimant comply with the commitments made to both parties and 
that it complies with the proposals that 
it has requested will be made to the labor bidder. 
That is, the labor plaintiff cannot create false expectations, 
deliver inaccurate or imprecise information, 
or promise conditions for the execution of 
the employment contract that will not be fulfilled.

** The labor bidder has the right to have all doubts he may have about a job 
offer made by the labor claimant before making a decision on whether or not 
to accept the offer submitted to him.

** The labor bidder has the right to be fulfilled by 
the labor claimant in the execution of the labor contract 
with all promises or offers regarding salaries, 
legal and extra-legal services, benefits, incentives, 
and in general everything that has been raised as the work scenario 
in which he would develop in case of accepting the job offer.

** The labor intermediary has the right to require the labor claimant to 
comply with what was promised or transmitted to 
the labor bidders during the selection process.

** The labor intermediary has the right to require the labor claimant to 
comply with what is agreed in the respective commercial agreement or 
service contract that binds the parties, given that real, truthful, 
timely information is delivered to salaries offered for each position or 
profile, on the annual income of the candidates that are hired and on the 
causes and motives that generate some type of claim by the labor claimant on 
the performance of the functions of the bidders of workforce.

** The labor bidder has the obligation both before 
the labor intermediary and the labor applicant to provide accurate, 
truthful information about his identity, 
basic information such as residence address, telephone numbers, address, age, 
studies courses, academic notes, special permits that you have to practice 
your profession or trade and work experience.

*8. OBLIGATIONS OF THE PROVIDER.*

[b f5]#AUTONOMIC S.A.S#. will have the following obligations:

** Make users aware of the Service Provision Regulation.

** Guarantee and enforce the rights of the owner of 
the information referred to in article 12 of this regulation.

** Provide free employment management and placement services 
to workers (suppliers).

** Guarantee in its actions the principles that inspire 
the Public Employment Service in accordance with the current legislation.

** Allow the correction or modification of the registered information of both 
bidders and claimants, a request that must be in writing, 
at the time in which they require it, 
in accordance with the provisions of Law 1581 of 2012.

** Ensure compliance with the agreed requirements in the services.

** To state in the development of the activities 
as a management and placement agency such condition, 
as well as in the means of promotion and dissemination of its activity, 
mentioning the number of the administrative act by which it was authorized, 
and membership in the Network of Providers of the Public Employment Service.

** Transmit the information registered by employers on the existence of 
vacancies to the Information System of the Public Employment Service, 
in the terms of Resolution No. 000129 of 2015 or the rules that regulate, 
add or reform.

** Present statistical reports on the management and placement of employment 
according to current regulations.

** Receive, attend and respond to complaints and claims submitted by users of 
the service as a management and placement agency, 
within fifteen (15) calendar days following receipt.

=== 9. VALUE OF THE COMMISSION OR FEES CHARGED BY THE INTERMEDIARY*
*TO THE MANUFACTURER DEMANDER FOR THEIR INTERMEDIATION.*

Initially, the value of the service is given by a multiplier of 2 times the 
monthly salary of the candidate. 
The cost of the service is generated for each candidate requested.

[b f5]#AUTONOMIC S.A.S.# reserves the right to modify the rate range when 
it considers it appropriate according to the needs of the service.

** 30% of the value of the service is non-refundable and must be received to 
start the search process for each vacancy. 
The remaining 70% is billed when the selected talent is hired. 
All invoices are with immediate payment term.

** If the person is removed by the company for breach of profile during a 
period of 120 calendar days from being hired, 
[b f5]#AUTONOMIC S.A.S.# performs another selection process at no cost 
seeking its replacement.

** If the candidate is hired for a different value from 
the one that was taken into account to quote, 
this change will be reflected in the 70% bill when the candidate is hired.

** There are discounts if you pay the entire investment before starting the 
service.

** The suspension of the project due to matters outside 
[b f5]#AUTONOMIC S.A.S# does not affect the billing and payments already 
generated. There is no money back.

** The client has seven (7) business days after sending the 2 candidates via 
email to decide whether or not the candidate continues the linking process, 
in case of not receiving a response within this period, 
it is considered that one of the candidates was selected to continue to the 
next stage of the process.

** [b f5]#AUTONOMIC S.A.S.# may present the rejected candidate immediately to 
other clients.

** Any talent referred by [b f5]#AUTONOMIC S.A.S.#, 
whether or not it is discarded in the selection process, 
and whether it is hired by the client within 6 months of the last decision 
made with the talent, counts as a service provided within the framework of 
the recruitment service.

** No service can be valued for a salary below COP $ 2,000,000 per month, 
and this monthly value is calculated with the candidate's annual salary 
divided by 12.

=== 10. ATTENTION OF PETITIONS, COMPLAINTS, CLAIMS – PQRSD.

Users of the service [b f5]#AUTONOMIC S.A.S.# as an employment management and 
placement agency may submit their requests, complaints and claims, 
which will be processed through the following procedure:
{nbsp} +
The user must communicate their comment, complaint or claim by writing to the 
email info@autonomicmind.com informing: full name, identity document, 
landline, cell phone, description of the event with subject, date, 
documentation that accounts for the subject and other supports required for 
analysis and attention.
{nbsp} +
[b f5]#AUTONOMIC S.A.S.# will respond to the communication within a period 
not exceeding fifteen (15) business days at the contact address and telephone 
numbers.
All observations that come through this service will be classified and 
documented to ensure an improvement in the service offered.
Upon receipt, [b f5]#AUTONOMIC S.A.S.# will begin the management of 
the petition, complaint or claim, establishing communication with 
the person who submitted it in order to respond and resolve the issue raised.

=== 11. LEGAL FRAMEWORK.

The management and placement services will be provided subject to the norms 
contained in these regulations, as provided in Law 1636 of June 18, 2013, 
in Decree 1072 of May 26, 2015 and in resolution 3999 of October 5 of 2015 
and the other norms that regulate labor intermediation in Colombia. 
Likewise, it will take care of compliance with Statutory Law 1581 of 2012 
through which general provisions for the protection of personal data are 
issued.

=== 12. INFORMATION MANAGEMENT.

For the operation and provision of 
employment management and placement services, 
[b f5]#AUTONOMIC S.A.S.# will have a computer system compatible and 
complementary to the Information System of the Public Employment Service, 
for the monthly supply, by electronic means, 
of the information on demands and offers of employment, 
as well as the rest of activities carried out as an agency.
This system will allow the registration of 
jobseekers and the required services; 
the traceability of the actions followed by 
the checks in relation to the service, the basic statistics, 
the knowledge of the resulting information, among other issues.

Likewise, within the first fifteen (15) days of the month, 
statistical reports are shared on the management and placement of employment 
made in the previous month, 
in the formats and by the means established by the competent authority.

Information related to the services will be on the website of 
[b f5]#AUTONOMIC S.A.S.#: autonomicmind.com
{nbsp} +
{nbsp} +
[.end]
== END OF THE SERVICE PROVISION REGULATIONS AS A MANAGEMENT AGENCY AND
[.end]
== PLACEMENT OF EMPLOYMENT OF AUTONOMIC S.A.S.
